package br.com.grupoLibra.boletimCarga.n4.repositories;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaObject;

@Component("ConsultasN4")
public class ConsultasN4 implements Serializable{
		
	private static final long serialVersionUID = -1081988244697430379L;

	@Autowired
	@Qualifier("n4EntityManagerFactory")
	private EntityManager entityManager;

	public CargoReportDTO buscaUnit(String gKey) {
		
	    String SQL =  "  SELECT  unit.gkey,                                                                                           "
		    		+ "          unit.id,                                                                                             "
		    		+ "          unit.seal_nbr1,                                                                                      "
		    		+ "          unit.seal_nbr2,                                                                                      "
		    		+ "          unit.seal_nbr3,                                                                                      "
		    		+ "          unit.seal_nbr4,                                                                                      "
		    		+ "          unit.flex_string13,                                                                                  "
		    		+ "          unit.flex_string14,                                                                                  "
		    		+ "          unit.flex_string15,                                                                                  "
		    		+ "          unit.freight_kind,                                                                                   "
		    		+ "          facility.restow_typ,                                                                                 "
		    		+ "          facility.FCY_GKEY,                                  										          "
		    		+ "          visit_detailsib.custdff_escalasiscomexcarga escalasiscomexcarga_ib,                                  "
		    		+ "          visit_detailsib.gkey cvcvd_gkey_ib,                                  								  "		    		
		    		+ "          visit_detailsob.custdff_escalasiscomexcarga escalasiscomexcarga_ob,								  "		    		  
		    		+ "          visit_detailsob.gkey cvcvd_gkey_ob, 																  "
		    		+ "          unitEquip.DAMAGE,																					  " 
		    		+ "          cargoLot.DMG_SEVERITY,																				  "  
		    		+ "          unit.PRIMARY_UE                                       										      	  "
		    		+ "  FROM    sparcsn4.inv_unit unit                                                                               "
		    		+ "  		   JOIN sparcsn4.inv_unit_fcy_visit facility  		ON (facility.unit_gkey=unit.gkey)                 "
		    		+ "  LEFT OUTER JOIN sparcsn4.argo_carrier_visit carrierIB 		ON (carrierib.gkey=facility.actual_ib_cv)         "
		    		+ "  		   JOIN sparcsn4.argo_visit_details visit_detailsIB ON (visit_detailsib.gkey=carrierib.cvcvd_gkey)    "
		    		+ "  LEFT OUTER JOIN sparcsn4.argo_carrier_visit carrierOB 		ON (carrierob.gkey=facility.actual_ob_cv)         "
		    		+ "  		   JOIN sparcsn4.argo_visit_details visit_detailsOB ON (visit_detailsob.gkey=carrierob.cvcvd_gkey)    "
		    		+ "  LEFT OUTER JOIN SPARCSN4.INV_UNIT_EQUIP unitEquip on (unitEquip.gkey=unit.PRIMARY_UE)						  "	
		    		+ "  LEFT OUTER JOIN SPARCSN4.CRG_LOTS cargoLot on (cargoLot.id=unit.ID) 										  "
		    		+ "  WHERE unit.gkey = "+gKey;
		    	
		Query nativeQuery = entityManager.createNativeQuery(SQL);
	    
		List<Object[]> lista = nativeQuery.getResultList();
		CargoReportDTO cargoReportDTO = new CargoReportDTO();
		cargoReportDTO.setTerminalOperation("BRRIO002");
		List<CargoReportUnitDTO> cargoReportUnitDTOs = new LinkedList<CargoReportUnitDTO>();
		List<CargoReportMovimentacaoMS> cargoReportMovimentacaoMSs = new LinkedList<CargoReportMovimentacaoMS>();
		for (Object[] object : lista) {
			int i = 0;
			Object[] array =(Object[]) object;			
			CargoReportUnitDTO cargoReportUnitDTO = new CargoReportUnitDTO();		 
			BigDecimal b = (BigDecimal) array[i++];
			cargoReportDTO.setGkey((Long)  b.setScale(0,BigDecimal.ROUND_UP).longValueExact());
			cargoReportUnitDTO.setUnitGkey(cargoReportDTO.getGkey());
			cargoReportUnitDTO.setUnitNbr((String) array[i++]);
			cargoReportUnitDTO.setSealNbr1((String) array[i++]);
			cargoReportUnitDTO.setSealNbr2((String) array[i++]);
			cargoReportUnitDTO.setSealNbr3((String) array[i++]);
			cargoReportUnitDTO.setSealNbr4((String) array[i++]);
			cargoReportUnitDTO.setUnitFlexString13((String) array[i++]);
			cargoReportUnitDTO.setLading(cargoReportUnitDTO.getUnitFlexString13());
			cargoReportUnitDTO.setUnitFlexString14((String) array[i++]);
			cargoReportUnitDTO.setNbrCargoItem(cargoReportUnitDTO.getUnitFlexString14());
			cargoReportUnitDTO.setUnitFlexString15((String) array[i++]);
			cargoReportUnitDTO.setManifest(cargoReportUnitDTO.getUnitFlexString15());
			cargoReportUnitDTO.setFreightKind((String) array[i++]);
			cargoReportUnitDTO.setTypeItem((cargoReportUnitDTO.getFreightKind().equals("BBK")) ? "2" : "1");
			cargoReportUnitDTO.setFacilityUfvRestowType((String) array[i++]);
			
			if (cargoReportUnitDTO.getFacilityUfvRestowType().equals("NONE")) {
				cargoReportUnitDTO.setTypeMovimentation("00");
			} else if (cargoReportUnitDTO.getFacilityUfvRestowType().equals("RESTOW")) {
				cargoReportUnitDTO.setTypeMovimentation("01");
			}
			b = (BigDecimal) array[i++];
			cargoReportDTO.setFacility((Long)  b.setScale(0,BigDecimal.ROUND_UP).longValueExact());
			
			
			String escala = (String) array[i++];			
			
			if (!ManipulaObject.isEmpty(escala) ) {
				cargoReportUnitDTO.setTypeOperation("D");
				cargoReportDTO.setEscalaSiscomexCarga(escala);
				BigDecimal bigDec = (BigDecimal) array[i++];
				cargoReportDTO.setVesselVisitDetails((Long)  bigDec.setScale(0,BigDecimal.ROUND_UP).longValueExact());				
				i++;
				i++;
			} else {				
				i++;				
				cargoReportUnitDTO.setTypeOperation("C");
				cargoReportDTO.setEscalaSiscomexCarga( (String) array[i++]);
				BigDecimal bigDec = (BigDecimal) array[i++];
				cargoReportDTO.setVesselVisitDetails((Long)  bigDec.setScale(0,BigDecimal.ROUND_UP).longValueExact());								
			}
			String avaria = " ";
			if(!cargoReportUnitDTO.getFreightKind().equals("BBK")){
				String result = (String) array[i++];
				if (result.equals("MAJOR")) {
					avaria = "S";
				} else if (result.equals("MINOR")) {
					avaria = "N";
				} 
				i++;
			} else {
				//cargoLot
				i++;
				String result = (String) array[i++];
				if (result.equals("MAJOR")) {
					avaria = "S";
				} else if (result.equals("MINOR")) {
					avaria = "N";
				}			
			}
			cargoReportUnitDTO.setIsDamage(avaria);
			cargoReportUnitDTO.setCreated(new Date());
			cargoReportUnitDTO.setCreator("msBoletim");			
			BigDecimal bigDec = (BigDecimal) array[i++];
			cargoReportUnitDTO.setPrimaryUe((Long)  bigDec.setScale(0,BigDecimal.ROUND_UP).longValueExact());
			cargoReportUnitDTO.setCargoReportDTO(cargoReportDTO);
			cargoReportUnitDTOs.add(cargoReportUnitDTO);
		}
		
		cargoReportDTO.setCargoReportUnits(cargoReportUnitDTOs);
		cargoReportDTO.setCargoReportMovimentacaoMS(cargoReportMovimentacaoMSs);
		return cargoReportDTO;
	}
	
	public boolean validateSeal(Long primaryue) {
		boolean validateSeal = false;
		
		String SQL = " SELECT  refEqDamage.id                                                     "
					+ " FROM   sparcsn4.inv_unit_equip unitEquip                                   "
					+ "        LEFT OUTER JOIN sparcsn4.inv_unit_equip_damages eqDamages           "
					+ "                     ON ( eqDamages.gkey = unitEquip.dmgs_gkey )            "
					+ "        LEFT OUTER JOIN sparcsn4.inv_unit_equip_damage_item eqDamagesItem   "
					+ "                     ON ( eqDamagesItem.dmgs_gkey = eqDamages.gkey )        "
					+ "        LEFT OUTER JOIN sparcsn4.ref_equip_damage_types refEqDamage         "
					+ "                     ON ( refEqDamage.gkey = eqDamagesItem.type_gkey )      "
					+ " WHERE  unitEquip.gkey = "+primaryue;
				
		
		Query nativeQuery = entityManager.createNativeQuery(SQL);
	    
		List<Object> lista = nativeQuery.getResultList();
		
		for (Object objects : lista) {
				 
			String itemType = (String) objects;			
			if(!ManipulaObject.isEmpty(itemType)) {
				if("01".equals(itemType)) {
					validateSeal = true;
				} else
				if("02".equals(itemType)) {
					validateSeal = true;
				} else
				if("LACRE".equals(itemType)) {
					validateSeal = true;
				} 				
			}
		}

		return validateSeal;
	}
		

	
}
