package br.com.grupoLibra.boletimCarga.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "n4EntityManagerFactory", 
					   transactionManagerRef = "n4TransactionManager", 
					   basePackages = {	"br.com.grupoLibra.boletimCarga.n4.repositories" })
public class BDN4Config {
	@Bean(name = "n4DataSource")
	@ConfigurationProperties(prefix = "n4.datasource")
	public DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUsername(ArquivoConfiguracao.getN4DatasourceUsername());
		driverManagerDataSource.setPassword(ArquivoConfiguracao.getN4DatasourcePassword());
		driverManagerDataSource.setUrl(ArquivoConfiguracao.getN4DatasourceUrl());									   
		driverManagerDataSource.setDriverClassName("oracle.jdbc.OracleDriver");
		return driverManagerDataSource;
	}
	
	@Bean(name = "n4EntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean n4EntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("n4DataSource") DataSource dataSource) {

		LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
		bean.setPackagesToScan("br.com.grupoLibra.boletimCarga.n4.models");
		bean.setDataSource(dataSource);
		 
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		bean.setJpaVendorAdapter(vendorAdapter);
		bean.setJpaProperties(aditionalPropetiesN4());
		bean.setPersistenceUnitName("n4EntityManagerFactory");
		
		return bean;
	}

	@Bean(name = "n4TransactionManager")
	public JpaTransactionManager barTransactionManager(
			@Qualifier("n4EntityManagerFactory") EntityManagerFactory n4EntityManagerFactory) {
		return new JpaTransactionManager(n4EntityManagerFactory);
	}
	
	@Bean
	public Properties aditionalPropetiesN4() {
		Properties properties = new Properties();		
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		properties.setProperty("hibernate.show_sql", ArquivoConfiguracao.getJpaShowSql());
		properties.setProperty("hibernate.hbm2ddl.auto", "update");				
		return properties;
	}	
}