package br.com.grupoLibra.boletimCarga.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

import br.com.grupoLibra.boletimCarga.messages.ListenerMessages;

@Configuration
@EnableJms
public class ListenerConfig {
	
	  @Bean
	  public ActiveMQConnectionFactory activeMQConnectionFactory() {
	    ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
	    activeMQConnectionFactory.setBrokerURL(ArquivoConfiguracao.getActiveMQBrokerUrl());

	    return activeMQConnectionFactory;
	  }

	  @Bean
	  public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
	    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	    factory.setConnectionFactory(activeMQConnectionFactory());
	    factory.setConcurrency("3-10");

	    return factory;
	  }

	  @Bean
	  public ListenerMessages receiver() {
	    return new ListenerMessages();
	  }	

}
