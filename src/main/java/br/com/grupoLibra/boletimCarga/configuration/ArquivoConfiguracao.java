package br.com.grupoLibra.boletimCarga.configuration;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.grupoLibra.boletimCarga.processos.ProcessoEnvioReceita;
import br.com.grupoLibra.boletimCarga.processos.Processos;

public class ArquivoConfiguracao {
	
	private static Properties prop;
	private static final Logger LOGGER = LoggerFactory.getLogger(Processos.class);
	
	public ArquivoConfiguracao() {
		
	}
	
	public static String getPathArquivoEntrada() {
		return getProp().getProperty("ms-boletimCarga-entrada");		
	}
	
	public static String getPathArquivoAssinados() {
		return getProp().getProperty("ms-boletimCarga-assinados");		
	}
	
	public static String getPathArquivoRetorno() {
		return getProp().getProperty("ms-boletimCarga-retorno");		
	}
	
	public static char[] getTrustStorePassword() {
		return getProp().getProperty("siscomex.trust-store-password").toCharArray();		
	}
	
	public static char[] getKeyStorePassword() {
		return getProp().getProperty("siscomex.key-store-password").toCharArray();		
	}
	
	public static String getKeyStore() {
		return getProp().getProperty("siscomex.key-store");		
	}
	
	public static String getActiveMQBrokerUrl() {
		return getProp().getProperty("spring.activemq.broker-url");		
	}
	
	public static String getN4DatasourceUrl() {
		return getProp().getProperty("n4.datasource.url");		
	}
	
	public static String getN4DatasourceUsername() {
		return getProp().getProperty("n4.datasource.username");		
	}
	
	public static String getN4DatasourcePassword() {
		return getProp().getProperty("n4.datasource.password");		
	}
	
	public static String getJpaShowSql() {
		return getProp().getProperty("spring.jpa.show-sql");		
	}
	
	public static String getCacerts() {
		return getProp().getProperty("siscomex.trust-cacerts");		
	}
	

	

	public static Properties getProp() {
		if (ArquivoConfiguracao.prop == null || ArquivoConfiguracao.prop.isEmpty()) {
			ArquivoConfiguracao.prop = new Properties();
			try {				
				ArquivoConfiguracao.prop.load(ProcessoEnvioReceita.class.getClassLoader().getResourceAsStream("application.properties"));
			} catch (IOException e) {
				LOGGER.error("Erro ao abrir arquivo de configuração --> ", e);
				e.printStackTrace();
			}
		}
		return ArquivoConfiguracao.prop;
	}

}
