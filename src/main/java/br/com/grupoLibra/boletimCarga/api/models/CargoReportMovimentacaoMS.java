package br.com.grupoLibra.boletimCarga.api.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CargoReportMovMS")
public class CargoReportMovimentacaoMS  implements Serializable {
	private static final long serialVersionUID = 1L;
		
	public static final String STATUSMS_INTEGRADO = "INTEGRADO";	
	public static final String STATUSMS_XML_ENTRADA = "XML-ENTRADA";
	public static final String STATUSMS_XML_ASSINADO = "XML-ASSINADO";
	public static final String STATUSMS_XML_RETORNO = "XML-RETORNO";
	public static final String STATUSMS_FALHA_ENVIO = "FALHA-ENVIO";	
	public static final String STATUSMS_ENVIO_RF = "ENVIO-RECEITA";
	public static final String STATUSMS_RETORNO_ERRO = "RETORNO-ERRO";
	public static final String STATUSMS_RETORNO = "RETORNO_RECEITA";
	public static final String STATUSMS_RETORNO_SUCESSO = "RETORNO_SUCESSO";
	public static final String STATUSMS_FALHA_POCESSO = "FALHA-PROCESSO-INTERNO";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="gkey_id")
	private CargoReportDTO cargoReportDTO;
	private Date dtOcorrencia;
	private String codigo;
    private String descricao;
    private String status;
    private String pathArquivo;    

	public CargoReportMovimentacaoMS() {
		
	}

	public CargoReportMovimentacaoMS(Long id, CargoReportDTO cargoReportDTO, Date dtOcorrencia, String codigo,
			String descricao, String status, String pathArquivo) {
		super();
		this.id = id;
		this.cargoReportDTO = cargoReportDTO;
		this.dtOcorrencia = dtOcorrencia;
		this.codigo = codigo;
		this.descricao = descricao;
		this.status = status;
		this.pathArquivo = pathArquivo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CargoReportDTO getCargoReportDTO() {
		return cargoReportDTO;
	}

	public void setCargoReportDTO(CargoReportDTO cargoReportDTO) {
		this.cargoReportDTO = cargoReportDTO;
	}

	public Date getDtOcorrencia() {
		return dtOcorrencia;
	}

	public void setDtOcorrencia(Date dtOcorrencia) {
		this.dtOcorrencia = dtOcorrencia;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPathArquivo() {
		return pathArquivo;
	}

	public void setPathArquivo(String pathArquivo) {
		this.pathArquivo = pathArquivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((dtOcorrencia == null) ? 0 : dtOcorrencia.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((pathArquivo == null) ? 0 : pathArquivo.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoReportMovimentacaoMS other = (CargoReportMovimentacaoMS) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (dtOcorrencia == null) {
			if (other.dtOcorrencia != null)
				return false;
		} else if (!dtOcorrencia.equals(other.dtOcorrencia))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pathArquivo == null) {
			if (other.pathArquivo != null)
				return false;
		} else if (!pathArquivo.equals(other.pathArquivo))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CargoReportMovimentacaoMS [id=" + id + ", dtOcorrencia=" + dtOcorrencia + ", codigo=" + codigo
				+ ", descricao=" + descricao + ", status=" + status + ", pathArquivo=" + pathArquivo + "]";
	}
	
		
	
}
