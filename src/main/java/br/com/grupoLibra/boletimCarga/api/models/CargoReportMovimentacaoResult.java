package br.com.grupoLibra.boletimCarga.api.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

public class CargoReportMovimentacaoResult  implements Serializable {
	private static final long serialVersionUID = 1L;
		
	private Long id;
	private Date dtOcorrencia;
	private String codigo;
    private String descricao;
    private String status;
    private String tipoMovimentacao;
	private Long vesselVisitDetails;

	public CargoReportMovimentacaoResult() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtOcorrencia() {
		return dtOcorrencia;
	}

	public void setDtOcorrencia(Date dtOcorrencia) {
		this.dtOcorrencia = dtOcorrencia;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTipoMovimentacao() {
		return tipoMovimentacao;
	}

	public void setTipoMovimentacao(String tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}

	public Long getVesselVisitDetails() {
		return vesselVisitDetails;
	}

	public void setVesselVisitDetails(Long vesselVisitDetails) {
		this.vesselVisitDetails = vesselVisitDetails;
	}

	@Override
	public String toString() {
		return "[CargoReportMovimentacaoResult:"
				+ "{\"id\":" + "\"" + id + "\"" + ","
			    + " \"dtOcorrencia\":" + "\"" + dtOcorrencia + "\"" + ", "
			    + "\"codigo\":" + "\"" + codigo + "\"" + ", "
			    + "\"descricao\":" + "\"" + descricao + "\"" + ", "
			    + "\"status\":" + "\"" + status + "\"" + ","
			    + " \"tipoMovimentacao\":" + "\"" + tipoMovimentacao + "\"" + ", "
			    + "\"vesselVisitDetails\":" + "\"" + vesselVisitDetails + "\"" + "}],";
	}

	
}
