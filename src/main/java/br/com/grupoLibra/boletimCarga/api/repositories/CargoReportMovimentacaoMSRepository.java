package br.com.grupoLibra.boletimCarga.api.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;

@Repository
public interface CargoReportMovimentacaoMSRepository extends JpaRepository<CargoReportMovimentacaoMS, Long>{
	
	//Selecionar pelo VesselVisit
	List<CargoReportMovimentacaoMS> findByCargoReportDTO_VesselVisitDetails(Long VesselVisitDetails);

	//Selecionar pelo Gkey
	List<CargoReportMovimentacaoMS> findByCargoReportDTO_Gkey(Long gkey);
	
	//Selecionar pelo Data Ocorrencia
	//@Query("from CargoReportUnitDTO where dtOcorrencia >= :dataIni and dtOcorrencia <= :dataFim")
	//List<CargoReportMovimentacaoMS> pesquisarPorDtOcorrencia(@Param("dataIni") Date dataIni, @Param("dataFim") Date dataFim);
	
	//Selecionar dtOcorrencia
	List<CargoReportMovimentacaoMS> findByDtOcorrenciaBetween(Date dataIni, Date dataFim);
	
	//Selecionar pelo Status
	List<CargoReportMovimentacaoMS> findByStatus(String status);

}
