package br.com.grupoLibra.boletimCarga.api.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.grupoLibra.boletimCarga.uteis.ManipulaObject;


@Entity
@Table(name = "CargoReportUnitDTO")
public class CargoReportUnitDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long 	id;
	
	private String 	typeItem;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="gkey_id")
	private CargoReportDTO cargoReportDTO;
	
	private String  freightKind;
	//inAvariasOcorrencia
	private String  isDamage;
	private String  lading;
	private String  manifest;
	private String  nbrCargoItem;
	private String  qtySeals;
	private String  sealNbr1;
	private String  sealNbr2;
	private String  sealNbr3;
	private String  sealNbr4;
	private String  typeMovimentation;
	private String  typeOperation;	
	//unitID
	private String  unitNbr;
	private Boolean itemStatus;
	
	@Temporal(TemporalType.DATE)
	private Date 	created;
	
	private String 	creator;
	private String 	cargoQuantity;
	private Long 	unitGkey;
	private String 	returnCode;
	
	private String 	facilityUfvRestowType;
	private String 	unitFlexString13;
	private String 	unitFlexString14;
	private String 	unitFlexString15;
	private Double 	unitCargoQuantity;
	
	private Long 	primaryUe;

	public CargoReportUnitDTO() {
	
	}

	public CargoReportUnitDTO(Long id, String typeItem, CargoReportDTO cargoReportDTO, String freightKind,
			String isDamage, String lading, String manifest, String nbrCargoItem, String qtySeals, String sealNbr1,
			String sealNbr2, String sealNbr3, String sealNbr4, String typeMovimentation, String typeOperation,
			String unitNbr, Boolean itemStatus, Date created, String creator, String cargoQuantity, Long unitGkey,
			String returnCode, String facilityUfvRestowType, String unitFlexString13, String unitFlexString14,
			String unitFlexString15, Double unitCargoQuantity) {
		super();
		this.id = id;
		this.typeItem = typeItem;
		this.cargoReportDTO = cargoReportDTO;
		this.freightKind = freightKind;
		this.isDamage = isDamage;
		this.lading = lading;
		this.manifest = manifest;
		this.nbrCargoItem = nbrCargoItem;
		this.qtySeals = qtySeals;
		this.sealNbr1 = sealNbr1;
		this.sealNbr2 = sealNbr2;
		this.sealNbr3 = sealNbr3;
		this.sealNbr4 = sealNbr4;
		this.typeMovimentation = typeMovimentation;
		this.typeOperation = typeOperation;
		this.unitNbr = unitNbr;
		this.itemStatus = itemStatus;
		this.created = created;
		this.creator = creator;
		this.cargoQuantity = cargoQuantity;
		this.unitGkey = unitGkey;
		this.returnCode = returnCode;
		this.facilityUfvRestowType = facilityUfvRestowType;
		this.unitFlexString13 = unitFlexString13;
		this.unitFlexString14 = unitFlexString14;
		this.unitFlexString15 = unitFlexString15;
		this.unitCargoQuantity = unitCargoQuantity;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTypeItem() {
		return typeItem;
	}
	public void setTypeItem(String typeItem) {
		this.typeItem = typeItem;
	}
	
	public String getFreightKind() {
		return freightKind;
	}
	public void setFreightKind(String freightKind) {
		this.freightKind = freightKind;
	}
	public String getIsDamage() {
		return isDamage;
	}
	public void setIsDamage(String isDamage) {
		this.isDamage = isDamage;
	}
	public String getLading() {
		return lading;
	}
	public void setLading(String lading) {
		this.lading = lading;
	}
	public String getManifest() {
		return manifest;
	}
	public void setManifest(String manifest) {
		this.manifest = manifest;
	}
	public String getNbrCargoItem() {
		return nbrCargoItem;
	}
	public void setNbrCargoItem(String nbrCargoItem) {
		this.nbrCargoItem = nbrCargoItem;
	}
	public String getQtySeals() {
		return qtySeals;
	}
	public void setQtySeals(String qtySeals) {
		this.qtySeals = qtySeals;
	}
	public String getSealNbr1() {
		return sealNbr1;
	}
	public void setSealNbr1(String sealNbr1) {
		if(!ManipulaObject.isEmpty(sealNbr1) && sealNbr1.length() > 15) {
			sealNbr1 = sealNbr1.substring(sealNbr1.length() - 15, sealNbr1.length() - 1);
		}
		
		this.sealNbr1 = sealNbr1;
	}
	public String getSealNbr2() {
		return sealNbr2;
	}
	public void setSealNbr2(String sealNbr2) {
		if(!ManipulaObject.isEmpty(sealNbr2) && sealNbr2.length() > 15) {
			sealNbr2 = sealNbr2.substring(sealNbr2.length() - 15, sealNbr2.length() - 1);
		}
		this.sealNbr2 = sealNbr2;
	}
	public String getSealNbr3() {
		return sealNbr3;
	}
	public void setSealNbr3(String sealNbr3) {
		if(!ManipulaObject.isEmpty(sealNbr3) && sealNbr3.length() > 15) {
			sealNbr3 = sealNbr3.substring(sealNbr3.length() - 15, sealNbr3.length() - 1);
		}
		this.sealNbr3 = sealNbr3;
	}
	public String getSealNbr4() {
		return sealNbr4;
	}
	public void setSealNbr4(String sealNbr4) {
		if(!ManipulaObject.isEmpty(sealNbr4) &&  sealNbr4.length() > 15) {
			sealNbr4 = sealNbr4.substring(sealNbr4.length() - 15, sealNbr4.length() - 1);
		}
		this.sealNbr4 = sealNbr4;
	}
	public String getTypeMovimentation() {
		return typeMovimentation;
	}
	public void setTypeMovimentation(String typeMovimentation) {
		this.typeMovimentation = typeMovimentation;
	}
	public String getTypeOperation() {
		return typeOperation;
	}
	public void setTypeOperation(String typeOperation) {
		this.typeOperation = typeOperation;
	}
	public String getUnitNbr() {
		return unitNbr;
	}
	public void setUnitNbr(String unitNbr) {
		this.unitNbr = unitNbr;
	}
	public Boolean getItemStatus() {
		return itemStatus;
	}
	public void setItemStatus(Boolean itemStatus) {
		this.itemStatus = itemStatus;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCargoQuantity() {
		return cargoQuantity;
	}
	public void setCargoQuantity(String cargoQuantity) {
		this.cargoQuantity = cargoQuantity;
	}
	public Long getUnitGkey() {
		return unitGkey;
	}
	public void setUnitGkey(Long unitGkey) {
		this.unitGkey = unitGkey;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getFacilityUfvRestowType() {
		return facilityUfvRestowType;
	}
	public void setFacilityUfvRestowType(String facilityUfvRestowType) {
		this.facilityUfvRestowType = facilityUfvRestowType;
	}
	public String getUnitFlexString13() {
		return unitFlexString13;
	}
	public void setUnitFlexString13(String unitFlexString13) {
		this.unitFlexString13 = unitFlexString13;
	}
	public String getUnitFlexString14() {
		return unitFlexString14;
	}
	public void setUnitFlexString14(String unitFlexString14) {
		this.unitFlexString14 = unitFlexString14;
	}
	public String getUnitFlexString15() {
		return unitFlexString15;
	}
	public void setUnitFlexString15(String unitFlexString15) {
		this.unitFlexString15 = unitFlexString15;
	}
	public Double getUnitCargoQuantity() {
		return unitCargoQuantity;
	}
	public void setUnitCargoQuantity(Double unitCargoQuantity) {
		this.unitCargoQuantity = unitCargoQuantity;
	}
	public CargoReportDTO getCargoReportDTO() {
		return cargoReportDTO;
	}
	public void setCargoReportDTO(CargoReportDTO cargoReportDTO) {
		this.cargoReportDTO = cargoReportDTO;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargoQuantity == null) ? 0 : cargoQuantity.hashCode());
		result = prime * result + ((cargoReportDTO == null) ? 0 : cargoReportDTO.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + ((facilityUfvRestowType == null) ? 0 : facilityUfvRestowType.hashCode());
		result = prime * result + ((freightKind == null) ? 0 : freightKind.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isDamage == null) ? 0 : isDamage.hashCode());
		result = prime * result + ((itemStatus == null) ? 0 : itemStatus.hashCode());
		result = prime * result + ((lading == null) ? 0 : lading.hashCode());
		result = prime * result + ((manifest == null) ? 0 : manifest.hashCode());
		result = prime * result + ((nbrCargoItem == null) ? 0 : nbrCargoItem.hashCode());
		result = prime * result + ((qtySeals == null) ? 0 : qtySeals.hashCode());
		result = prime * result + ((returnCode == null) ? 0 : returnCode.hashCode());
		result = prime * result + ((sealNbr1 == null) ? 0 : sealNbr1.hashCode());
		result = prime * result + ((sealNbr2 == null) ? 0 : sealNbr2.hashCode());
		result = prime * result + ((sealNbr3 == null) ? 0 : sealNbr3.hashCode());
		result = prime * result + ((sealNbr4 == null) ? 0 : sealNbr4.hashCode());
		result = prime * result + ((typeItem == null) ? 0 : typeItem.hashCode());
		result = prime * result + ((typeMovimentation == null) ? 0 : typeMovimentation.hashCode());
		result = prime * result + ((typeOperation == null) ? 0 : typeOperation.hashCode());
		result = prime * result + ((unitCargoQuantity == null) ? 0 : unitCargoQuantity.hashCode());
		result = prime * result + ((unitFlexString13 == null) ? 0 : unitFlexString13.hashCode());
		result = prime * result + ((unitFlexString14 == null) ? 0 : unitFlexString14.hashCode());
		result = prime * result + ((unitFlexString15 == null) ? 0 : unitFlexString15.hashCode());
		result = prime * result + ((unitGkey == null) ? 0 : unitGkey.hashCode());
		result = prime * result + ((unitNbr == null) ? 0 : unitNbr.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoReportUnitDTO other = (CargoReportUnitDTO) obj;
		if (cargoQuantity == null) {
			if (other.cargoQuantity != null)
				return false;
		} else if (!cargoQuantity.equals(other.cargoQuantity))
			return false;
		if (cargoReportDTO == null) {
			if (other.cargoReportDTO != null)
				return false;
		} else if (!cargoReportDTO.equals(other.cargoReportDTO))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (facilityUfvRestowType == null) {
			if (other.facilityUfvRestowType != null)
				return false;
		} else if (!facilityUfvRestowType.equals(other.facilityUfvRestowType))
			return false;
		if (freightKind == null) {
			if (other.freightKind != null)
				return false;
		} else if (!freightKind.equals(other.freightKind))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isDamage == null) {
			if (other.isDamage != null)
				return false;
		} else if (!isDamage.equals(other.isDamage))
			return false;
		if (itemStatus == null) {
			if (other.itemStatus != null)
				return false;
		} else if (!itemStatus.equals(other.itemStatus))
			return false;
		if (lading == null) {
			if (other.lading != null)
				return false;
		} else if (!lading.equals(other.lading))
			return false;
		if (manifest == null) {
			if (other.manifest != null)
				return false;
		} else if (!manifest.equals(other.manifest))
			return false;
		if (nbrCargoItem == null) {
			if (other.nbrCargoItem != null)
				return false;
		} else if (!nbrCargoItem.equals(other.nbrCargoItem))
			return false;
		if (qtySeals == null) {
			if (other.qtySeals != null)
				return false;
		} else if (!qtySeals.equals(other.qtySeals))
			return false;
		if (returnCode == null) {
			if (other.returnCode != null)
				return false;
		} else if (!returnCode.equals(other.returnCode))
			return false;
		if (sealNbr1 == null) {
			if (other.sealNbr1 != null)
				return false;
		} else if (!sealNbr1.equals(other.sealNbr1))
			return false;
		if (sealNbr2 == null) {
			if (other.sealNbr2 != null)
				return false;
		} else if (!sealNbr2.equals(other.sealNbr2))
			return false;
		if (sealNbr3 == null) {
			if (other.sealNbr3 != null)
				return false;
		} else if (!sealNbr3.equals(other.sealNbr3))
			return false;
		if (sealNbr4 == null) {
			if (other.sealNbr4 != null)
				return false;
		} else if (!sealNbr4.equals(other.sealNbr4))
			return false;
		if (typeItem == null) {
			if (other.typeItem != null)
				return false;
		} else if (!typeItem.equals(other.typeItem))
			return false;
		if (typeMovimentation == null) {
			if (other.typeMovimentation != null)
				return false;
		} else if (!typeMovimentation.equals(other.typeMovimentation))
			return false;
		if (typeOperation == null) {
			if (other.typeOperation != null)
				return false;
		} else if (!typeOperation.equals(other.typeOperation))
			return false;
		if (unitCargoQuantity == null) {
			if (other.unitCargoQuantity != null)
				return false;
		} else if (!unitCargoQuantity.equals(other.unitCargoQuantity))
			return false;
		if (unitFlexString13 == null) {
			if (other.unitFlexString13 != null)
				return false;
		} else if (!unitFlexString13.equals(other.unitFlexString13))
			return false;
		if (unitFlexString14 == null) {
			if (other.unitFlexString14 != null)
				return false;
		} else if (!unitFlexString14.equals(other.unitFlexString14))
			return false;
		if (unitFlexString15 == null) {
			if (other.unitFlexString15 != null)
				return false;
		} else if (!unitFlexString15.equals(other.unitFlexString15))
			return false;
		if (unitGkey == null) {
			if (other.unitGkey != null)
				return false;
		} else if (!unitGkey.equals(other.unitGkey))
			return false;
		if (unitNbr == null) {
			if (other.unitNbr != null)
				return false;
		} else if (!unitNbr.equals(other.unitNbr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CargoReportUnitDTO [id=" + id + ", typeItem=" + typeItem + ", freightKind=" + freightKind
				+ ", isDamage=" + isDamage + ", lading=" + lading + ", manifest=" + manifest + ", nbrCargoItem="
				+ nbrCargoItem + ", qtySeals=" + qtySeals + ", sealNbr1=" + sealNbr1 + ", sealNbr2=" + sealNbr2
				+ ", sealNbr3=" + sealNbr3 + ", sealNbr4=" + sealNbr4 + ", typeMovimentation=" + typeMovimentation
				+ ", typeOperation=" + typeOperation + ", unitNbr=" + unitNbr + ", itemStatus=" + itemStatus
				+ ", created=" + created + ", creator=" + creator + ", cargoQuantity=" + cargoQuantity + ", unitGkey="
				+ unitGkey + ", returnCode=" + returnCode + ", facilityUfvRestowType=" + facilityUfvRestowType
				+ ", unitFlexString13=" + unitFlexString13 + ", unitFlexString14=" + unitFlexString14
				+ ", unitFlexString15=" + unitFlexString15 + ", unitCargoQuantity=" + unitCargoQuantity + "]";
	}

	public Long getPrimaryUe() {
		return primaryUe;
	}

	public void setPrimaryUe(Long primaryUe) {
		this.primaryUe = primaryUe;
	}
	
}
