package br.com.grupoLibra.boletimCarga.api.models;

import java.util.List;

public class CargoReportResult {
	private String boletimId;
	private String vesselVisitId;
	private String escalaSiscomex;
	private String dataEnvioFormatada;
	private String dataRetornoFormatada;
	private String serpro;
	private String qtdeItens;
	private String operacao;
	private String restow;
	private String status;
	private Long boletimGkey;
	private List<CargoReportResultUnit> cargoReportResultUnit;

	public String getBoletimId() {
		return boletimId;
	}
	public void setBoletimId(String boletimId) {
		this.boletimId = boletimId;
	}
	public String getVesselVisitId() {
		return vesselVisitId;
	}
	public void setVesselVisitId(String vesselVisitId) {
		this.vesselVisitId = vesselVisitId;
	}
	public String getEscalaSiscomex() {
		return escalaSiscomex;
	}
	public void setEscalaSiscomex(String escalaSiscomex) {
		this.escalaSiscomex = escalaSiscomex;
	}
	public String getDataEnvioFormatada() {
		return dataEnvioFormatada;
	}
	public void setDataEnvioFormatada(String dataEnvioFormatada) {
		this.dataEnvioFormatada = dataEnvioFormatada;
	}
	public String getDataRetornoFormatada() {
		return dataRetornoFormatada;
	}
	public void setDataRetornoFormatada(String dataRetornoFormatada) {
		this.dataRetornoFormatada = dataRetornoFormatada;
	}
	public String getSerpro() {
		return serpro;
	}
	public void setSerpro(String serpro) {
		this.serpro = serpro;
	}
	public String getQtdeItens() {
		return qtdeItens;
	}
	public void setQtdeItens(String qtdeItens) {
		this.qtdeItens = qtdeItens;
	}
	public String getOperacao() {
		return operacao;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public String getRestow() {
		return restow;
	}
	public void setRestow(String restow) {
		this.restow = restow;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getBoletimGkey() {
		return boletimGkey;
	}
	public void setBoletimGkey(Long boletimGkey) {
		this.boletimGkey = boletimGkey;
	}
	public List<CargoReportResultUnit> getCargoReportResultUnit() {
		return cargoReportResultUnit;
	}
	public void setCargoReportResultUnit(List<CargoReportResultUnit> cargoReportResultUnit) {
		this.cargoReportResultUnit = cargoReportResultUnit;
	}

	@Override
	public String toString() {
		return "CargoReportResult [boletimId=" + boletimId + ", vesselVisitId=" + vesselVisitId + ", escalaSiscomex="
				+ escalaSiscomex + ", dataEnvioFormatada=" + dataEnvioFormatada + ", dataRetornoFormatada="
				+ dataRetornoFormatada + ", serpro=" + serpro + ", qtdeItens=" + qtdeItens + ", operacao=" + operacao
				+ ", restow=" + restow + ", status=" + status + ", boletimGkey=" + boletimGkey
				+ ", cargoReportResultUnit=" + cargoReportResultUnit + "]";
	}

}
