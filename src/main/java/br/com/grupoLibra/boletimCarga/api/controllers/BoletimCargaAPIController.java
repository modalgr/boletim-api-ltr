package br.com.grupoLibra.boletimCarga.api.controllers;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoResult;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportResult;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportResultUnit;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
import br.com.grupoLibra.boletimCarga.api.repositories.CargoReportDTORepository;
import br.com.grupoLibra.boletimCarga.api.repositories.CargoReportMovimentacaoMSRepository;
import br.com.grupoLibra.boletimCarga.api.repositories.CargoReportUnitDTORepository;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaJson;
import net.shibboleth.utilities.java.support.service.ServiceException;

/*
 * @author ModalGR - Ronaldo Freitas
 * @since  10/2017
 *  
 * Controller de acesso a todas as requisições as tabelas CargoReport da API
 */

@RestController
@RequestMapping("/rest")
public class BoletimCargaAPIController {

	@Autowired
	private CargoReportDTORepository cargoReportDTORepository;

	@Autowired
	private CargoReportUnitDTORepository cargoReportUnitDTORepository;

	@Autowired
	private CargoReportMovimentacaoMSRepository cargoReportMovimentacaoMSRepository;

	@RequestMapping(value="/cargoReportMovimentacoes", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CargoReportMovimentacaoMS> cadastrarCargoMovimento(@RequestBody CargoReportMovimentacaoMS cargoReportMovimentacao) {
		
		cargoReportMovimentacaoMSRepository.save(cargoReportMovimentacao); 
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Transactional
	@RequestMapping(value = "/cargoReportMovimentacoes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> findMovimentacaoTodos() throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {

		List<CargoReportMovimentacaoMS> listaCargoReportDTOMovimentacaoEncontrados = new ArrayList<>();
		List<CargoReportMovimentacaoResult> listaCargoReportDTOMovimentacaoVolta = new ArrayList<>();
		List<CargoReportUnitDTO> listaCargoReportUniDTO = new ArrayList<>();

		CargoReportUnitDTO cargoReportUniDTO = new CargoReportUnitDTO();
		
/*		Gson gson = new Gson();
*/		Gson gson = new GsonBuilder()
			       .setDateFormat("dd/MM/yyyy")
			       .create();
		
		listaCargoReportDTOMovimentacaoEncontrados = cargoReportMovimentacaoMSRepository.findAll();
		
		for (CargoReportMovimentacaoMS cargoReportMS : listaCargoReportDTOMovimentacaoEncontrados) {

			listaCargoReportUniDTO = cargoReportUnitDTORepository.findByCargoReportDTO_Gkey(cargoReportMS.getCargoReportDTO().getGkey());
			cargoReportUniDTO = listaCargoReportUniDTO.get(0);
			listaCargoReportDTOMovimentacaoVolta.add(ManipulaJson.geraCargoReportMovimentacao(cargoReportMS,cargoReportUniDTO.getTypeOperation().toString()));
		}
		
		System.out.println("RDF - Lista Volta : " + listaCargoReportDTOMovimentacaoVolta.toString());
		return new ResponseEntity<>(gson.toJson(listaCargoReportDTOMovimentacaoVolta), HttpStatus.OK);
	}

	@Transactional
	@RequestMapping(value = "/cargoReportMovimentacoes/{tipoDeAcesso}/{chaveDeAcesso}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> findMovimentacao(@PathVariable String tipoDeAcesso, @PathVariable String chaveDeAcesso) 
			throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {

		List<CargoReportMovimentacaoMS> listaCargoReportDTOMovEncontrado = new ArrayList<>();
		List<CargoReportUnitDTO> listaCargoReportUniDTO = new ArrayList<>();
		List<CargoReportMovimentacaoResult> listaCargoReportDTOMovimentacaoVolta = new ArrayList<>();

		/*		Gson gson = new Gson();
		*/		Gson gson = new GsonBuilder()
					       .setDateFormat("dd/MM/yyyy")
					       .create();

/*		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
*/		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

		Date dataIni = null;
		Date dataFim = null;

		CargoReportUnitDTO cargoReportUniDTO = new CargoReportUnitDTO();

		String dataFormatada;
		String dataFormIni;
		String dataFormFim;
		
		
		if (tipoDeAcesso.equals("3")) {
			try {
				dataFormatada = chaveDeAcesso;
				dataFormIni = dataFormatada + " 00:00:00";
				dataFormFim = dataFormatada + " 23:59:59";
				dataIni = formato.parse(dataFormIni);
				dataFim = formato.parse(dataFormFim);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		switch(tipoDeAcesso) {

		case "1" :
			listaCargoReportDTOMovEncontrado = cargoReportMovimentacaoMSRepository.findByCargoReportDTO_VesselVisitDetails(Long.parseLong(chaveDeAcesso)) ;
			break;

		case "2" :
			listaCargoReportDTOMovEncontrado = cargoReportMovimentacaoMSRepository.findByCargoReportDTO_Gkey(Long.parseLong(chaveDeAcesso)) ;
			break;
			
		case "3" :
			listaCargoReportDTOMovEncontrado = cargoReportMovimentacaoMSRepository.findByDtOcorrenciaBetween(dataIni, dataFim) ;
			System.out.println("RDF  - Acesso lista cargo " + listaCargoReportDTOMovEncontrado);
			break;

		case "4" :
			listaCargoReportDTOMovEncontrado = cargoReportMovimentacaoMSRepository.findByStatus(chaveDeAcesso);
			break;

		default :
			return new ResponseEntity<>(gson.toJson("NOTFOUND"), HttpStatus.NOT_FOUND);
		}

		for (CargoReportMovimentacaoMS cargoReportMS : listaCargoReportDTOMovEncontrado) {

			listaCargoReportUniDTO = cargoReportUnitDTORepository.findByCargoReportDTO_Gkey(cargoReportMS.getCargoReportDTO().getGkey());
			cargoReportUniDTO = listaCargoReportUniDTO.get(0);
			listaCargoReportDTOMovimentacaoVolta.add(ManipulaJson.geraCargoReportMovimentacao(cargoReportMS,cargoReportUniDTO.getTypeOperation().toString()));
		}
		
		return new ResponseEntity<>(gson.toJson(listaCargoReportDTOMovimentacaoVolta), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/pesquisarVesselVisit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> pesquisarVesselVisit() throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {
	
		List<CargoReportDTO> listaCargoReportEncontrados = new ArrayList<>();
		List<String> listaVesselVisitString = new ArrayList<String>();
		
		listaCargoReportEncontrados = cargoReportDTORepository.findAll();

		for (CargoReportDTO cargoReport : listaCargoReportEncontrados) {
			listaVesselVisitString.add(ManipulaJson.geraVesselVisit(cargoReport));
		}
		return new ResponseEntity<>(listaVesselVisitString.toString(), HttpStatus.OK);
	}
	
	@Transactional
	@RequestMapping(value = "/findVesselVisitInfo/{tipoDeAcesso}/{chaveDeAcesso}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> findVesselVisitInfoComChave(@PathVariable String tipoDeAcesso, @PathVariable String chaveDeAcesso) 
			throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {

		String tipoMontagemArquivo = null;
		List<CargoReportDTO> listaCargoReportEncontrados = new ArrayList<>();
		List<CargoReportUnitDTO> listaCargoReportUnitEncontrados = new ArrayList<>();
		List<CargoReportResult> listaCargoReportResultVolta = new ArrayList<>();
		/*		Gson gson = new Gson();
		*/		Gson gson = new GsonBuilder()
					       .setDateFormat("dd/MM/yyyy")
					       .create();
		
		switch(tipoDeAcesso) {

		case "1" :
			listaCargoReportEncontrados = cargoReportDTORepository.findByIdPackageXML(chaveDeAcesso);
			tipoMontagemArquivo = "CRD";
			break;

		case "2" :
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByUnitNbr(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;
			
		case "3" :
			listaCargoReportEncontrados = cargoReportDTORepository.findByEscalaSiscomexCarga(chaveDeAcesso);
			tipoMontagemArquivo = "CRD";
			break;

		case "4" :
			listaCargoReportEncontrados = cargoReportDTORepository.findByUnitQuantity(chaveDeAcesso);
			tipoMontagemArquivo = "CRD";			
			break;

		case "5" :
		listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.pesquisarSealNbr(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;

		case "6" :
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByFreightKind(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;

		case "7" :
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByCargoQuantity(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;

		case "8" :
			if (chaveDeAcesso.equalsIgnoreCase("Sucesso")) {
				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByItemStatusTrue();
			} else if (chaveDeAcesso.equalsIgnoreCase("Erro")){
//              Nao existe este tipo status foi alterado para string e uma outra visao. nao existe mais pacote	status = "Falso";
//				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.pesquisarStatusCargoReportDTO(status);
			} else {

//				status = "True";
//				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.pesquisarItemStatusEStatus(status);
			}
			tipoMontagemArquivo = "CRU";
			break;

		case "9" :
			if (chaveDeAcesso.equalsIgnoreCase("Load")) {
				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByTypeOperation("C");
			} else {
				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByTypeOperation("D");
			}
			tipoMontagemArquivo = "CRU";
			break;
			
		case "10" :
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByManifest(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;

		case "11" :
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByLading(chaveDeAcesso);
			tipoMontagemArquivo = "CRU";
			break;

		case "12" :
			if (chaveDeAcesso.equalsIgnoreCase("Restow")) {
				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByFacilityUfvRestowType("None");
			} else {
				listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByFacilityUfvRestowTypeNot("None");
			}
			tipoMontagemArquivo = "CRU";
			break;

		case "13" :
			String dano;
			if (chaveDeAcesso.equalsIgnoreCase("Minor")) {
				dano = "N";
			} else if (chaveDeAcesso.equalsIgnoreCase("Major")){
				dano = "S";
			} else {
				dano = " ";
			}
			listaCargoReportUnitEncontrados = cargoReportUnitDTORepository.findByIsDamage(dano);
			tipoMontagemArquivo = "CRU";
			break;
			
		default :
			tipoMontagemArquivo = "NOTFOUND";
		}

		if (tipoMontagemArquivo == "CRD") {
			for (CargoReportDTO cargoReport : listaCargoReportEncontrados) {
				listaCargoReportResultVolta.add(ManipulaJson.geraCargoReportDTOParaCargoReportResult(cargoReport));
			}
		}else if (tipoMontagemArquivo == "CRU"){
			for (CargoReportUnitDTO cargoReportUnit : listaCargoReportUnitEncontrados) {
				listaCargoReportResultVolta.add(ManipulaJson.geraCargoReportUnitDTOParaCargoReportResult(cargoReportUnit));
			}
		} 

		return new ResponseEntity<>(gson.toJson(listaCargoReportResultVolta), HttpStatus.OK);
	}

	@Transactional
	@RequestMapping(value = "/findVesselVisitInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> findVesselVisitInfo() throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {
		
		List<CargoReportDTO> listaCargoReportEncontrados = new ArrayList<>();
		List<CargoReportResult> listaCargoReportResultVolta = new ArrayList<>();

		listaCargoReportEncontrados = cargoReportDTORepository.findAll();

		/*		Gson gson = new Gson();
		*/		Gson gson = new GsonBuilder()
					       .setDateFormat("dd/MM/yyyy")
					       .create();

		
		for (CargoReportDTO cargoReport : listaCargoReportEncontrados) {
			listaCargoReportResultVolta.add(ManipulaJson.geraCargoReportDTOParaCargoReportResult(cargoReport));
		}
		
		return new ResponseEntity<>(gson.toJson(listaCargoReportResultVolta), HttpStatus.OK);
	}


	@RequestMapping(value="/apicargo", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CargoReportDTO> cadastrarCRD(@RequestBody CargoReportDTO cargoReportDTO) {

		cargoReportDTORepository.save(cargoReportDTO); 
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/apicargo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> buscarTodosCRD() throws MalformedURLException, RemoteException, ServiceException, JsonProcessingException {
	
		List<CargoReportDTO> listaCargoReportEncontrados = new ArrayList<>();
		List<String> listaCargoReportString = new ArrayList<String>();
		
		listaCargoReportEncontrados = cargoReportDTORepository.findAll();

		for (CargoReportDTO cargoReport : listaCargoReportEncontrados) {
			listaCargoReportString.add(ManipulaJson.geraCargoReport(cargoReport));
		}
		
		return new ResponseEntity<>(listaCargoReportString.toString(), HttpStatus.OK);
	}
	

	@RequestMapping(value="/apicargo/{gKey}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> buscarCRD(@PathVariable Long gKey) throws JsonProcessingException,MalformedURLException, RemoteException, ServiceException  {

		CargoReportDTO crdBuscado = cargoReportDTORepository.findOne(gKey); 
		if (crdBuscado == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(ManipulaJson.geraCargoReport(crdBuscado), HttpStatus.OK);
		}
	}

	@RequestMapping(value="/apicargo/{gKey}",method=RequestMethod.DELETE)
	public ResponseEntity<CargoReportDTO> excluirCRD(@PathVariable Long gKey) {

		CargoReportDTO crdEncontrado = cargoReportDTORepository.findOne(gKey);
		
		
		if (crdEncontrado != null){
			cargoReportDTORepository.delete(crdEncontrado); 
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(method=RequestMethod.PUT, value="/apicargo", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CargoReportDTO> alterarCRD(@RequestBody CargoReportDTO cargoReportDTO) {

		cargoReportDTORepository.save(cargoReportDTO); 
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
	
