package br.com.grupoLibra.boletimCarga.api.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CargoReportDTO")
public class CargoReportDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String STATUS_INTEGRADO_N4 = "INTEGRADO_N4";
	public static final String STATUS_INTEGRADO_XML = "INTEGRADO_XML";
	
	@Id
	private Long gkey;

	@Temporal(TemporalType.DATE)
	private Date dateDispatch;

	@Temporal(TemporalType.DATE)
	private Date dateReturn;

	private Long facility;
	private String idPackageXML;
	private String status;
	private String terminalOperation;
	private String unitQuantity;
	private String escalaSiscomexCarga;
	private Long vesselVisitDetails;

	@Temporal(TemporalType.DATE)
	private Date created;

	private String creator;

	@OneToMany(mappedBy = "cargoReportDTO", cascade = CascadeType.ALL)
	private List<CargoReportUnitDTO> cargoReportUnits;

	@OneToMany(mappedBy = "cargoReportDTO", cascade = CascadeType.ALL)
	private List<CargoReportMovimentacaoMS> cargoReportMovimentacaoMSs;

	public CargoReportDTO() {

	}

	public CargoReportDTO(Long gkey, Date dateDispatch, Date dateReturn, Long facility, String idPackageXML,
			String status, String terminalOperation, String unitQuantity, String escalaSiscomexCarga,
			Long vesselVisitDetails, Date created, String creator, List<CargoReportUnitDTO> cargoReportUnits,
			List<CargoReportMovimentacaoMS> cargoReportMovimentacaoMSs) {
		super();
		this.gkey = gkey;
		this.dateDispatch = dateDispatch;
		this.dateReturn = dateReturn;
		this.facility = facility;
		this.idPackageXML = idPackageXML;
		this.status = status;
		this.terminalOperation = terminalOperation;
		this.unitQuantity = unitQuantity;
		this.escalaSiscomexCarga = escalaSiscomexCarga;
		this.vesselVisitDetails = vesselVisitDetails;
		this.created = created;
		this.creator = creator;
		this.cargoReportUnits = cargoReportUnits;
		this.cargoReportMovimentacaoMSs = cargoReportMovimentacaoMSs;
	}

	public Long getGkey() {
		return gkey;
	}

	public void setGkey(Long gkey) {
		this.gkey = gkey;
	}

	public Date getDateDispatch() {
		return dateDispatch;
	}

	public void setDateDispatch(Date dateDispatch) {
		this.dateDispatch = dateDispatch;
	}

	public Date getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}

	public Long getFacility() {
		return facility;
	}

	public void setFacility(Long facility) {
		this.facility = facility;
	}

	public String getIdPackageXML() {
		return idPackageXML;
	}

	public void setIdPackageXML(String idPackageXML) {
		this.idPackageXML = idPackageXML;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTerminalOperation() {
		return terminalOperation;
	}

	public void setTerminalOperation(String terminalOperation) {
		this.terminalOperation = terminalOperation;
	}

	public String getUnitQuantity() {
		return unitQuantity;
	}

	public void setUnitQuantity(String unitQuantity) {
		this.unitQuantity = unitQuantity;
	}

	public String getEscalaSiscomexCarga() {
		return escalaSiscomexCarga;
	}

	public void setEscalaSiscomexCarga(String escalaSiscomexCarga) {
		this.escalaSiscomexCarga = escalaSiscomexCarga;
	}

	public Long getVesselVisitDetails() {
		return vesselVisitDetails;
	}

	public void setVesselVisitDetails(Long vesselVisitDetails) {
		this.vesselVisitDetails = vesselVisitDetails;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public List<CargoReportUnitDTO> getCargoReportUnits() {
		return cargoReportUnits;
	}

	public void setCargoReportUnits(List<CargoReportUnitDTO> cargoReportUnits) {
		this.cargoReportUnits = cargoReportUnits;
	}

	public List<CargoReportMovimentacaoMS> getCargoReportMovimentacaoMS() {
		return cargoReportMovimentacaoMSs;
	}

	public void setCargoReportMovimentacaoMS(List<CargoReportMovimentacaoMS> cargoReportMovimentacaoMSs) {
		this.cargoReportMovimentacaoMSs = cargoReportMovimentacaoMSs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargoReportMovimentacaoMSs == null) ? 0 : cargoReportMovimentacaoMSs.hashCode());
		result = prime * result + ((cargoReportUnits == null) ? 0 : cargoReportUnits.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + ((dateDispatch == null) ? 0 : dateDispatch.hashCode());
		result = prime * result + ((dateReturn == null) ? 0 : dateReturn.hashCode());
		result = prime * result + ((escalaSiscomexCarga == null) ? 0 : escalaSiscomexCarga.hashCode());
		result = prime * result + ((facility == null) ? 0 : facility.hashCode());
		result = prime * result + ((gkey == null) ? 0 : gkey.hashCode());
		result = prime * result + ((idPackageXML == null) ? 0 : idPackageXML.hashCode());				
		result = prime * result + ((terminalOperation == null) ? 0 : terminalOperation.hashCode());
		result = prime * result + ((unitQuantity == null) ? 0 : unitQuantity.hashCode());
		result = prime * result + ((vesselVisitDetails == null) ? 0 : vesselVisitDetails.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoReportDTO other = (CargoReportDTO) obj;
		if (cargoReportMovimentacaoMSs == null) {
			if (other.cargoReportMovimentacaoMSs != null)
				return false;
		} else if (!cargoReportMovimentacaoMSs.equals(other.cargoReportMovimentacaoMSs))
			return false;
		if (cargoReportUnits == null) {
			if (other.cargoReportUnits != null)
				return false;
		} else if (!cargoReportUnits.equals(other.cargoReportUnits))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (dateDispatch == null) {
			if (other.dateDispatch != null)
				return false;
		} else if (!dateDispatch.equals(other.dateDispatch))
			return false;
		if (dateReturn == null) {
			if (other.dateReturn != null)
				return false;
		} else if (!dateReturn.equals(other.dateReturn))
			return false;
		if (escalaSiscomexCarga == null) {
			if (other.escalaSiscomexCarga != null)
				return false;
		} else if (!escalaSiscomexCarga.equals(other.escalaSiscomexCarga))
			return false;
		if (facility == null) {
			if (other.facility != null)
				return false;
		} else if (!facility.equals(other.facility))
			return false;
		if (gkey == null) {
			if (other.gkey != null)
				return false;
		} else if (!gkey.equals(other.gkey))
			return false;
		if (idPackageXML == null) {
			if (other.idPackageXML != null)
				return false;
		} else if (!idPackageXML.equals(other.idPackageXML))
			return false;		
		if (terminalOperation == null) {
			if (other.terminalOperation != null)
				return false;
		} else if (!terminalOperation.equals(other.terminalOperation))
			return false;
		if (unitQuantity == null) {
			if (other.unitQuantity != null)
				return false;
		} else if (!unitQuantity.equals(other.unitQuantity))
			return false;
		if (vesselVisitDetails == null) {
			if (other.vesselVisitDetails != null)
				return false;
		} else if (!vesselVisitDetails.equals(other.vesselVisitDetails))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CargoReportDTO [gkey=" + gkey + ", dateDispatch=" + dateDispatch + ", dateReturn=" + dateReturn
				+ ", facility=" + facility + ", idPackageXML=" + idPackageXML + ", status=" + status
				+ ", terminalOperation=" + terminalOperation + ", unitQuantity=" + unitQuantity
				+ ", escalaSiscomexCarga=" + escalaSiscomexCarga + ", vesselVisitDetails=" + vesselVisitDetails
				+ ", created=" + created + ", creator=" + creator + "]";
		// + ", cargoReportUnits="+ cargoReportUnits.toString() + ", cargoReportEnvios="
		// + cargoReportEnvios.toString() + "]";

	}
}
