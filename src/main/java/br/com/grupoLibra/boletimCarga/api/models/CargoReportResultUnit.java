package br.com.grupoLibra.boletimCarga.api.models;

public class CargoReportResultUnit {

	private Long   unitBoletimGkey;
	private String unitNbr;
	private String seals;
	private String cargoQty;
	private String freightKind;
	private String operacao;
	private String manifesto;
	private String ceMercante;
	private String itemSiscomex;
	private String restow;
	private String damage;
	private String status;

	public Long getUnitBoletimGkey() {
		return unitBoletimGkey;
	}
	public void setUnitBoletimGkey(Long unitBoletimGkey) {
		this.unitBoletimGkey = unitBoletimGkey;
	}
	public String getUnitNbr() {
		return unitNbr;
	}
	public void setUnitNbr(String unitNbr) {
		this.unitNbr = unitNbr;
	}
	public String getSeals() {
		return seals;
	}
	public void setSeals(String seals) {
		this.seals = seals;
	}
	public String getCargoQty() {
		return cargoQty;
	}
	public void setCargoQty(String cargoQty) {
		this.cargoQty = cargoQty;
	}
	public String getFreightKind() {
		return freightKind;
	}
	public void setFreightKind(String freightKind) {
		this.freightKind = freightKind;
	}
	public String getOperacao() {
		return operacao;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public String getManifesto() {
		return manifesto;
	}
	public void setManifesto(String manifesto) {
		this.manifesto = manifesto;
	}
	public String getCeMercante() {
		return ceMercante;
	}
	public void setCeMercante(String ceMercante) {
		this.ceMercante = ceMercante;
	}
	public String getItemSiscomex() {
		return itemSiscomex;
	}
	public void setItemSiscomex(String itemSiscomex) {
		this.itemSiscomex = itemSiscomex;
	}
	public String getRestow() {
		return restow;
	}
	public void setRestow(String restow) {
		this.restow = restow;
	}
	public String getDamage() {
		return damage;
	}
	public void setDamage(String damage) {
		this.damage = damage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "CargoReportResultUnit [unitBoletimGkey=" + unitBoletimGkey + ", unitNbr=" + unitNbr + ", seals=" + seals
				+ ", cargoQty=" + cargoQty + ", freightKind=" + freightKind + ", operacao=" + operacao + ", manifesto="
				+ manifesto + ", ceMercante=" + ceMercante + ", itemSiscomex=" + itemSiscomex + ", restow=" + restow
				+ ", damage=" + damage + ", status=" + status + "]";
	}
	
	
}
