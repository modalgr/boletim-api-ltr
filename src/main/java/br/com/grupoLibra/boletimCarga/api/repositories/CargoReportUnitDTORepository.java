package br.com.grupoLibra.boletimCarga.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;

/*
 * @author ModalGR - Ronaldo Freitas
 * @since 10/2017
 * 
 * Acessos a entidade CargoReportUnitDTO
 * 
 */

@Repository
public interface CargoReportUnitDTORepository extends JpaRepository<CargoReportUnitDTO, Long>{

	//Selecionar pelo Container(unitNbr)
	List<CargoReportUnitDTO> findByUnitNbr(String unitNbr);
	
	//Selecionar por lacre(sealNbr)
	@Query("from CargoReportUnitDTO where sealNbr1 = :sealNbr or sealNbr2 = :sealNbr or sealNbr3 = :sealNbr or sealNbr4 = :sealNbr")
	List<CargoReportUnitDTO> pesquisarSealNbr(@Param("sealNbr") String sealNbr);
	
	//Selecionar por freightKind
	List<CargoReportUnitDTO> findByFreightKind(String freightKind);
	
	//Selecionar por cargoQuantity
	List<CargoReportUnitDTO> findByCargoQuantity(String cargoQuantity);

	//Selecionar por lacre(sealNbr)
	List<CargoReportUnitDTO> findByItemStatusTrue();

	//Selecionar por Operacao
	List<CargoReportUnitDTO> findByTypeOperation(String typeOperation);
	
	//Selecionar por Manifesto
	List<CargoReportUnitDTO> findByManifest(String manifest);

	//Selecionar por CE(lading)
	List<CargoReportUnitDTO> findByLading(String lading);
	
	//Selecionar por Restow(facilityUfvRestowType)
	List<CargoReportUnitDTO> findByFacilityUfvRestowType(String facilityUfvRestowType);

	//Selecionar por Restow(facilityUfvRestowType) negando a chave
	List<CargoReportUnitDTO> findByFacilityUfvRestowTypeNot(String facilityUfvRestowType);
	
	//Selecionar por isDamage
	List<CargoReportUnitDTO> findByIsDamage(String isDamage);
	
	//Selecionar pelo Gkey CargoReportDTO
	List<CargoReportUnitDTO> findByCargoReportDTO_Gkey(Long gkey);

}





