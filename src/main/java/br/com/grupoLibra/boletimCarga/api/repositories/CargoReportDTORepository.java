package br.com.grupoLibra.boletimCarga.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;

@Repository
public interface CargoReportDTORepository extends JpaRepository<CargoReportDTO, Long>{
	
	//Selecionar pelo BoletimID(IdPackageXML)
	List<CargoReportDTO> findByIdPackageXML(String idPackageXML);

	//Selecionar pelo escalaSiscomex(escalaSiscomexCarga)
	List<CargoReportDTO> findByEscalaSiscomexCarga(String escalaSiscomexCarga);

	//Selecionar pelo QtdItens(unitQuantity)
	List<CargoReportDTO> findByUnitQuantity(String unitQuantity);
	
}
