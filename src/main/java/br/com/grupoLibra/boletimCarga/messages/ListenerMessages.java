package br.com.grupoLibra.boletimCarga.messages;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import br.com.grupoLibra.boletimCarga.configuration.ArquivoConfiguracao;
import br.com.grupoLibra.boletimCarga.processos.ProcessoEnvioReceita;
import br.com.grupoLibra.boletimCarga.processos.ProcessoRetornoReceitaFederal;
import br.com.grupoLibra.boletimCarga.processos.Processos;

@Component
public class ListenerMessages {
	private static final Logger LOGGER = LoggerFactory.getLogger(Processos.class);

	public static final String FILA_ENVIO_RF = "ms-BoletimCarga-EnvioReceitaFederal.queue";
	public static final String FILA_ASSINATURA = "ms-BoletimCarga-Assinatura.queue";
	public static final String FILA_RETORNO = "ms-BoletimCarga-RetornoReceitaFederal.queue";
	public static final String FILA_N4 = "ms-BoletimCarga-UnitN4.queue";

	public static final String FILA_GERAXML = "ms-BoletimCarga-GeraXML.queue";

	@Autowired
	private ProcessoEnvioReceita processoEnvioReceita;

	@Autowired
	private ProcessoRetornoReceitaFederal processoRetornoReceitaFederal;

	@Autowired
	private Processos processos;

	@JmsListener(destination = FILA_GERAXML)
	public void listenerGerarXML(final Message jsonMessage) {
		// {"unitGkey":"188798391"}  
		try {
			String messageData = null;
			if (jsonMessage instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) jsonMessage;
				messageData = textMessage.getText();
				LOGGER.info("Recebe fila='{}' Mensagem='{}'", FILA_GERAXML, textMessage.getText());
				Map map = new Gson().fromJson(messageData, Map.class);

				processos.processoPreparacaoEnvio((String) map.get("unitGkey"));
			}
		} catch (JsonSyntaxException e) {
			LOGGER.error("ERRO mensagem recebida não está em formato JSON. Fila " + FILA_GERAXML + "--> "+ e.getMessage());
		} catch (JMSException e) {
			LOGGER.error("ERRO mensageria Fila " + FILA_GERAXML + "--> "+ e.getMessage());			
		}

	}

	@JmsListener(destination = FILA_N4)
	public void listenerN4(final Message jsonMessage) {
		try {
			String messageData = null;
			if (jsonMessage instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) jsonMessage;
				LOGGER.info("Recebe fila='{}' Mensagem='{}'", FILA_N4, textMessage.getText());
				messageData = textMessage.getText();
				Map map = new Gson().fromJson(messageData, Map.class);
				processos.processoIntegracao((String) map.get("unitGkey"));
			}
		} catch (JsonSyntaxException e) {
			LOGGER.error("ERRO mensagem recebida não está em formato JSON. Fila " + FILA_N4 + "--> "+ e.getMessage());
		} catch (JMSException e) {
			LOGGER.error("ERRO mensageria Fila " + FILA_N4 + "--> "+ e.getMessage());			
		}
	}

	@JmsListener(destination = FILA_ASSINATURA)
	public void listenerAssinatura(final Message jsonMessage) {
		// {"pathArquivo":"C:\\boletim\\entrada\\190453376.xml" ,"unitGkey":"162016050"}
		// {"pathArquivo":"C:\\boletim\\entrada\\12-11-2017-04-05-14_190453376.xml" ,"unitGkey":"190453376"}
		
		
		try {
			String messageData = null;
			if (jsonMessage instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) jsonMessage;
				messageData = textMessage.getText();
				LOGGER.info("Recebe fila='{}' Mensagem='{}'", FILA_ASSINATURA, textMessage.getText());
				Map map = new Gson().fromJson(messageData, Map.class);
				processos.processoPreparacaoEnvio((String) map.get("pathArquivo"), (String) map.get("unitGkey"));
			}
		} catch (JsonSyntaxException e) {
			LOGGER.error("ERRO mensagem recebida não está em formato JSON. Fila " + FILA_ASSINATURA + "--> "+ e.getMessage());
		} catch (JMSException e) {
			LOGGER.error("ERRO mensageria Fila " + FILA_ASSINATURA + "--> "+ e.getMessage());			
		}
	}

	@JmsListener(destination = FILA_ENVIO_RF)
	public void listenerEnvioRF(final Message jsonMessage) throws JMSException {
		try {
			String messageData = null;
			if (jsonMessage instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) jsonMessage;
				messageData = textMessage.getText();
				LOGGER.info("Recebe fila='{}' Mensagem='{}'", FILA_ENVIO_RF, textMessage.getText());
				Map map = new Gson().fromJson(messageData, Map.class);
				processoEnvioReceita.execute((String) map.get("pathArquivo"), (String) map.get("unitGkey"));
			}
		} catch (JsonSyntaxException e) {
			LOGGER.error("ERRO mensagem recebida não está em formato JSON. Fila " + FILA_ENVIO_RF + "--> "+ e.getMessage());
		} catch (JMSException e) {
			LOGGER.error("ERRO mensageria Fila " + FILA_ENVIO_RF + "--> "+ e.getMessage());			
		}

	}

	@JmsListener(destination = FILA_RETORNO)
	public void listenerRetorno(final Message jsonMessage) {
		try {
			String messageData = null;

			if (jsonMessage instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) jsonMessage;
				messageData = textMessage.getText();
				LOGGER.info("Recebe fila='{}' Mensagem='{}'", FILA_RETORNO, textMessage.getText());
				Map map = new Gson().fromJson(messageData, Map.class);
				processoRetornoReceitaFederal.execute((String) map.get("pathArquivo"), (String) map.get("unitGkey"));
			}
		} catch (JsonSyntaxException e) {
			LOGGER.error("ERRO mensagem recebida não está em formato JSON. Fila " + FILA_RETORNO + "--> "+ e.getMessage());
		} catch (JMSException e) {
			LOGGER.error("ERRO mensageria Fila " + FILA_RETORNO + "--> "+ e.getMessage());			
		}

	}

	public static void send(String destination, String message) {
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
		activeMQConnectionFactory.setBrokerURL(ArquivoConfiguracao.getActiveMQBrokerUrl());
		CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(activeMQConnectionFactory);
		JmsTemplate jmsTemplate = new JmsTemplate(cachingConnectionFactory);
		LOGGER.info("Envia fila='{}' Mensagem='{}'", destination, message);
		jmsTemplate.convertAndSend(destination, message);
	}

}