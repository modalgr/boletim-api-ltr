
package br.com.grupoLibra.boletimCarga.ws.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de cargaDescarga complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="cargaDescarga">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xmlCargaDescargaWSVO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cargaDescarga", propOrder = {
    "xmlCargaDescargaWSVO"
})
public class CargaDescarga {

    protected String xmlCargaDescargaWSVO;

    /**
     * Obt�m o valor da propriedade xmlCargaDescargaWSVO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlCargaDescargaWSVO() {
        return xmlCargaDescargaWSVO;
    }

    /**
     * Define o valor da propriedade xmlCargaDescargaWSVO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlCargaDescargaWSVO(String value) {
        this.xmlCargaDescargaWSVO = value;
    }

}
