package br.com.grupoLibra.boletimCarga.ws.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import org.springframework.stereotype.Service;

import br.com.grupoLibra.boletimCarga.configuration.ArquivoConfiguracao;

@Service("CertificadoA1")
public class CertificadoA1 {
	private X509Certificate x509Certificate;
	private KeyStore keyStore;
	private PublicKey publicKey;
	private PrivateKey privateKey;

	public CertificadoA1()
			throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
		this.openKeyStore();
	}

	public void openKeyStore()
			throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
		this.criaKeyStore();
		this.privateKey = (PrivateKey) this.keyStore.getKey(this.keyStore.aliases().nextElement(),
				ArquivoConfiguracao.getTrustStorePassword());
		setX509Certificate((X509Certificate) this.keyStore.getCertificate(this.keyStore.aliases().nextElement()));
		this.publicKey = getX509Certificate().getPublicKey();
	}

	private void criaKeyStore() {
		File fileCertificado = new File(ArquivoConfiguracao.getKeyStore());
		File fileCertificadoOut = new File(ArquivoConfiguracao.getKeyStore() + "boletimCarga.jks");

		if (!fileCertificado.canRead()) {
			System.err.println("Erro path Certificado: " + fileCertificado.getPath());
			System.exit(2);
		}
		if (fileCertificadoOut.exists() && !fileCertificadoOut.canWrite()) {
			System.err.println("Erro path Certificado saida:  " + fileCertificadoOut.getPath());
			System.exit(2);
		}
		KeyStore keyStoreSpkcs12;
		try {
			keyStoreSpkcs12 = KeyStore.getInstance("pkcs12");

			KeyStore keyStorejks = KeyStore.getInstance("jks");
			char[] senhaCertificado = ArquivoConfiguracao.getKeyStorePassword();
			char[] senhaCertificadoNew = ArquivoConfiguracao.getTrustStorePassword();
			keyStoreSpkcs12.load(new FileInputStream(fileCertificado), senhaCertificado);
			keyStorejks.load((fileCertificadoOut.exists()) ? new FileInputStream(fileCertificadoOut) : null,
					senhaCertificadoNew);
			Enumeration enumerationAliases = keyStoreSpkcs12.aliases();
			int n = 0;
			while (enumerationAliases.hasMoreElements()) {
				String strAlias = (String) enumerationAliases.nextElement();
				System.err.println("Alias " + n++ + ": " + strAlias);
				if (keyStoreSpkcs12.isKeyEntry(strAlias)) {
					System.err.println("Adding key for alias " + strAlias);
					Key key = keyStoreSpkcs12.getKey(strAlias, senhaCertificado);
					Certificate[] chain = keyStoreSpkcs12.getCertificateChain(strAlias);
					keyStorejks.setKeyEntry("001", key, senhaCertificadoNew, chain);
				}
			}
			OutputStream out = new FileOutputStream(fileCertificadoOut);
			keyStorejks.store(out, senhaCertificadoNew);
			out.close();
			this.keyStore = keyStorejks;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException
				| UnrecoverableKeyException e) {
			System.out.println("Erro ao criar certificado auxiliar");
			e.printStackTrace();
		}
	}

	public static String getValidade(X509Certificate cert) {
		try {
			cert.checkValidity();
			return "Certificado válido!";
		} catch (CertificateExpiredException e) {
			return "Certificado expirado!";
		} catch (CertificateNotYetValidException e) {
			return "Certificado inválido!";
		}
	}

	public KeyStore getKeyStore() {
		return keyStore;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public X509Certificate getX509Certificate() {
		return x509Certificate;
	}

	public void setX509Certificate(X509Certificate x509Certificate) {
		this.x509Certificate = x509Certificate;
	}

	/*
	 * catch(CertificateExpiredException ex){ //
	 * fireMessage("\n\nCERTIFICADO EXPIRADO EM: " + new
	 * DateTime(c.getNotAfter()).toString("dd/MM/yyyy HH:mm:ss'h'"), listener);
	 * return false; } catch(CertificateNotYetValidException ex){ //
	 * fireMessage("CERTIFICADO VÁLIDO SOMENTE A PARTIR DE: " + new
	 * DateTime(c.getNotBefore()).toString("dd/MM/yyyy HH:mm:ss'h'"), listener);
	 * return false; }
	 * 
	 * // dias= Days.daysBetween(Utils.getDateTime(), new
	 * DateTime(c.getNotAfter())).getDays(); //
	 * fireMessage("Certificado válido até: " + new
	 * DateTime(c.getNotAfter()).toString("dd/MM/yyyy HH:mm:ss'h'"), listener);
	 */
}
