
package br.com.grupoLibra.boletimCarga.ws.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.serpro.siscomex.carga.webservice.cargadescarga package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CargaDescargaResponse_QNAME = new QName("http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", "cargaDescargaResponse");
    private final static QName _CargaDescarga_QNAME = new QName("http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", "cargaDescarga");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.serpro.siscomex.carga.webservice.cargadescarga
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CargaDescargaResponse }
     * 
     */
    public CargaDescargaResponse createCargaDescargaResponse() {
        return new CargaDescargaResponse();
    }

    /**
     * Create an instance of {@link CargaDescarga }
     * 
     */
    public CargaDescarga createCargaDescarga() {
        return new CargaDescarga();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargaDescargaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", name = "cargaDescargaResponse")
    public JAXBElement<CargaDescargaResponse> createCargaDescargaResponse(CargaDescargaResponse value) {
        return new JAXBElement<CargaDescargaResponse>(_CargaDescargaResponse_QNAME, CargaDescargaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargaDescarga }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", name = "cargaDescarga")
    public JAXBElement<CargaDescarga> createCargaDescarga(CargaDescarga value) {
        return new JAXBElement<CargaDescarga>(_CargaDescarga_QNAME, CargaDescarga.class, null, value);
    }

}
