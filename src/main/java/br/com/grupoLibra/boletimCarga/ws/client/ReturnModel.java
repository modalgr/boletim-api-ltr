package br.com.grupoLibra.boletimCarga.ws.client;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="cargaDescargaWSResponse", namespace="http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/")
public class ReturnModel
{
  private String codigo;
  private String protocolo;
  private String descricao;
  
  public String getCodigo()
  {
    return this.codigo;
  }
  
  public void setCodigo(String codigo)
  {
    this.codigo = codigo;
  }
  
  public String getProtocolo()
  {
    return this.protocolo;
  }
  
  public void setProtocolo(String protocolo)
  {
    this.protocolo = protocolo;
  }
  
  public String getDescricao()
  {
    return this.descricao;
  }
  
  public void setDescricao(String descricao)
  {
    this.descricao = descricao;
  }
  
  public String toString()
  {
    return "ClassPojo [codigo = " + this.codigo + ", protocolo = " + this.protocolo + ", descricao = " + this.descricao + "]";
  }
}
