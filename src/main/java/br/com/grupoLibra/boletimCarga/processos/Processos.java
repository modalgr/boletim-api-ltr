package br.com.grupoLibra.boletimCarga.processos;

import java.io.IOException;
import java.io.Serializable;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.repositories.CargoReportDTORepository;
import br.com.grupoLibra.boletimCarga.configuration.ArquivoConfiguracao;
import br.com.grupoLibra.boletimCarga.messages.ListenerMessages;
import br.com.grupoLibra.boletimCarga.services.BuscaDadosN4Service;
import br.com.grupoLibra.boletimCarga.services.PersisteDadosBDService;
import br.com.grupoLibra.boletimCarga.services.PreparaCargaDescargaWSVOService;
import br.com.grupoLibra.boletimCarga.services.RegrasService;
import br.com.grupoLibra.boletimCarga.services.XMLAssinaturaService;
import br.com.grupoLibra.boletimCarga.services.XMLService;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaJson;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaObject;
import br.com.grupoLibra.boletimCarga.ws.security.CertificadoA1;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO;

@Service("Processos")
public class Processos implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger LOGGER = LoggerFactory.getLogger(Processos.class);

	@Autowired
	private CargoReportDTORepository cargoReportDTORepository;

	@Autowired
	private PersisteDadosBDService persisteDadosBDService;

	@Autowired
	private PreparaCargaDescargaWSVOService preparaCargaDescargaWSVOService;

	@Autowired
	private XMLService xMLService;

	@Autowired
	private BuscaDadosN4Service buscaDadosN4Service;

	@Autowired
	private RegrasService processoRegra;

	@Autowired
	private CertificadoA1 tokenReader;

	@Autowired
	private XMLAssinaturaService xMLAssinaturaService;

	public String processoIntegracao(String gKey) {
		CargoReportDTO cargoReportDTO = buscaDadosN4Service.execute(gKey);
		if (processoRegra.verifique(cargoReportDTO)) {
			persisteDadosBDService.gravaCargoReportDTO(cargoReportDTO, CargoReportMovimentacaoMS.STATUSMS_INTEGRADO);
			try {
				// Gera Arquivo XML - Entrada e Assina
				return processoPreparacaoEnvio(cargoReportDTO);
			
			} catch (MarshalException e) {
				LOGGER.error("ERRO Processo Integração ao criar arquivo assinado "+ e.getMessage());		
			} catch (XMLSignatureException e) {
				LOGGER.error("ERRO Processo Integração  ao criar arquivo assinado " + e.getMessage());
			} catch (TransformerException e) {
				LOGGER.error("ERRO Processo Integração  ao criar arquivo assinado " + e.getMessage());			
			} catch (NoSuchAlgorithmException e) {
				LOGGER.error("ERRO Processo Integração Token " + e.getMessage());			
			} catch (SAXException e) {
				LOGGER.error("ERRO Processo Integração " + e.getMessage());		
			} catch (ParserConfigurationException e) {
				LOGGER.error("ERRO Processo Integração " + e.getMessage());			
			} catch (IOException e) {
				LOGGER.error("ERRO Processo Integração ao abrir arquivos "+ e.getMessage());			
			} catch (KeyStoreException e) {
				LOGGER.error("ERRO Processo Integração ao abrir Token "+ e.getMessage());
			} catch (UnrecoverableKeyException e) {
				LOGGER.error("ERRO Processo Integração ao abrir Token "+ e.getMessage());
			} 
		}
		return "";

	}

	private String processoPreparacaoEnvio(CargoReportDTO cargoReportDTO)
			throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, SAXException,
			ParserConfigurationException, MarshalException, XMLSignatureException, TransformerException, IOException {
		// geraObjeto e arquivo XML
		CargaDescargaWSVO cargaDescargaWSVO = preparaCargaDescargaWSVOService.execute(cargoReportDTO);		
		String pathArquivo = xMLService.execute(cargoReportDTO, cargaDescargaWSVO,
				ArquivoConfiguracao.getPathArquivoEntrada());
		
		persisteDadosBDService.gravaCargoReportMovimentacaoMS(cargoReportDTO.getGkey().toString(),
				CargoReportMovimentacaoMS.STATUSMS_XML_ENTRADA, pathArquivo);

		// Assinar
		String pathArquivoAssinado = xMLAssinaturaService.execute(tokenReader.getX509Certificate(),
				tokenReader.getPrivateKey(), pathArquivo, cargoReportDTO.getGkey().toString());
		persisteDadosBDService.gravaCargoReportMovimentacaoMS(cargoReportDTO.getGkey().toString(),
				CargoReportMovimentacaoMS.STATUSMS_XML_ASSINADO, pathArquivoAssinado);

		ListenerMessages.send(ListenerMessages.FILA_ENVIO_RF, ManipulaJson.geraMensagemEnvioFila(
				cargoReportDTO.getCargoReportUnits().get(0).getUnitGkey().toString(), pathArquivoAssinado));

		return ManipulaJson.geraMensagemEnvioFila(cargoReportDTO.getCargoReportUnits().get(0).getUnitGkey().toString(),
				pathArquivoAssinado);
	}

	public String processoPreparacaoEnvio(String pathArquivoEntrada, String unitGkey) {

		// Assinar
		String pathArquivoAssinado, retorno = null;
		try {
			pathArquivoAssinado = xMLAssinaturaService.execute(tokenReader.getX509Certificate(),
					tokenReader.getPrivateKey(), pathArquivoEntrada, unitGkey);
			persisteDadosBDService.gravaCargoReportMovimentacaoMS(unitGkey, CargoReportMovimentacaoMS.STATUSMS_XML_ASSINADO,
					pathArquivoAssinado);
			ListenerMessages.send(ListenerMessages.FILA_ENVIO_RF,
					ManipulaJson.geraMensagemEnvioFila(unitGkey, pathArquivoAssinado));

			retorno = ManipulaJson.geraMensagemEnvioFila(unitGkey, pathArquivoAssinado);
		} catch (MarshalException e) {
			LOGGER.error("ERRO Processo Assinatura ao criar arquivo assinado "+ e.getMessage());		
		} catch (XMLSignatureException e) {
			LOGGER.error("ERRO Processo Assinatura  ao criar arquivo assinado " + e.getMessage());
		} catch (TransformerException e) {
			LOGGER.error("ERRO Processo Assinatura  ao criar arquivo assinado " + e.getMessage());			
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("ERRO Processo Assinatura Token " + e.getMessage());			
		} catch (SAXException e) {
			LOGGER.error("ERRO Processo Assinatura " + e.getMessage());		
		} catch (ParserConfigurationException e) {
			LOGGER.error("ERRO Processo Assinatura " + e.getMessage());			
		} catch (IOException e) {
			LOGGER.error("ERRO Processo Assinatura ao abrir arquivos "+ e.getMessage());			
		}
		return retorno;
	}

	@Transactional
	public String processoPreparacaoEnvio(String unitGkey) {
		String pathArquivo = "";
		// geraObjeto e arquivo XML
		CargoReportDTO cargoReportDTO = cargoReportDTORepository.findOne(Long.parseLong(unitGkey));

		if ((cargoReportDTO != null && 
				!ManipulaObject.isEmpty(cargoReportDTO)) && 
				( cargoReportDTO.getCargoReportUnits() != null 
				&& cargoReportDTO.getCargoReportUnits().size() > 0 )) {
			CargaDescargaWSVO cargaDescargaWSVO = preparaCargaDescargaWSVOService.execute(cargoReportDTO);
			pathArquivo = xMLService.execute(cargoReportDTO, cargaDescargaWSVO,
					ArquivoConfiguracao.getPathArquivoEntrada());
			persisteDadosBDService.gravaCargoReportMovimentacaoMS(cargoReportDTO,
					CargoReportMovimentacaoMS.STATUSMS_XML_ENTRADA, pathArquivo);

			try {
				ListenerMessages.send(ListenerMessages.FILA_ASSINATURA,
						ManipulaJson.geraMensagemEnvioFila(unitGkey, pathArquivo));
			} catch (JsonProcessingException e) {
				LOGGER.error("Processo Gerar XML Manual - Erro Interno em gerar Json para envio na fila --> "+e);	
				e.printStackTrace();
			}
		} else {
			LOGGER.error("Processo Gerar XML Manual - O UnitGkey não localizado na base de dados ou dados encontrados insuficiente para gerar arquivo XML");		}
		return pathArquivo;
	}
}
