package br.com.grupoLibra.boletimCarga.processos;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.configuration.ArquivoConfiguracao;
import br.com.grupoLibra.boletimCarga.messages.ListenerMessages;
import br.com.grupoLibra.boletimCarga.services.PersisteDadosBDService;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaArquivos;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaDatas;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaJson;
import br.com.grupoLibra.boletimCarga.ws.client.CargaDescargaWS;

@org.springframework.stereotype.Service("ProcessoEnvioReceita")
public class ProcessoEnvioReceita {
	private static final Logger LOGGER = LoggerFactory.getLogger(Processos.class);
	
	@Autowired	
	private PersisteDadosBDService persisteDadosBDService;

	public String execute(String localArquivo, String unitGKey) {
		String retorno = null;
		
		try {
			File fileAssinado = new File(localArquivo);
			
			System.setProperty("javax.net.ssl.trustStore",ArquivoConfiguracao.getCacerts());
			URL url = new URL("https://www4.receita.fazenda.gov.br/carga-ws/cargadescarga");
			
			QName qname = new QName("http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", "cargadescarga");
			
			Service service = Service.create(url, qname);
			
			CargaDescargaWS bc = (CargaDescargaWS) service.getPort(CargaDescargaWS.class);
			
			persisteDadosBDService.gravaCargoReportMovimentacaoMS(unitGKey, CargoReportMovimentacaoMS.STATUSMS_ENVIO_RF, localArquivo);
			
			retorno = bc.cargaDescarga(new String(Files.readAllBytes(Paths.get(localArquivo, new String[0]))));
			
			String arquivoRetorno = ManipulaArquivos.gravaArquivo(ArquivoConfiguracao.getPathArquivoRetorno()+ManipulaDatas.getCompNomeArquivo() + unitGKey+".xml", retorno);
			
			persisteDadosBDService.gravaCargoReportMovimentacaoMS(unitGKey, CargoReportMovimentacaoMS.STATUSMS_RETORNO, arquivoRetorno);
			
			ListenerMessages.send(ListenerMessages.FILA_RETORNO, ManipulaJson.geraMensagemEnvioFila(unitGKey,	arquivoRetorno));
			
		} catch (Exception e) {
			LOGGER.error("ERRO Processo Envio para Receita falha no envio "+ e.getMessage());			
		}		
		
		return retorno;
	}
	
}
