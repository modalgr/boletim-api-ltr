package br.com.grupoLibra.boletimCarga.processos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.services.PersisteDadosBDService;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaObject;
import br.com.grupoLibra.boletimCarga.ws.client.ReturnModel;

@Service("ProcessoRetornoReceitaFederal")
public class ProcessoRetornoReceitaFederal {
	
	@Autowired	
	private PersisteDadosBDService persisteDadosBDService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Processos.class);
 
	public String execute(String localArquivo, String unitGKey) {
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(localArquivo);
			JAXBContext context = JAXBContext.newInstance(new Class[] { ReturnModel.class });
			
			Reader reader = new InputStreamReader(inputStream, "UTF-8");
			
			Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
			ReturnModel ret = (ReturnModel) jaxbUnmarshaller.unmarshal(reader);
			
			CargoReportMovimentacaoMS cargoReportMovimentacaoMS = new CargoReportMovimentacaoMS();
			cargoReportMovimentacaoMS.setPathArquivo(localArquivo);			
			cargoReportMovimentacaoMS.setDtOcorrencia(new DateTime().toDate());	
			cargoReportMovimentacaoMS.setCodigo(ret.getCodigo());
			cargoReportMovimentacaoMS.setDescricao(ret.getDescricao());			
			
			if (!ManipulaObject.isEmpty(ret.getCodigo()) && ret.getCodigo().equals("CA0000")) {
				cargoReportMovimentacaoMS.setStatus(CargoReportMovimentacaoMS.STATUSMS_RETORNO_SUCESSO);								
			} else {
				cargoReportMovimentacaoMS.setStatus(CargoReportMovimentacaoMS.STATUSMS_RETORNO_ERRO);				
			}
			persisteDadosBDService.gravaCargoReportMovimentacaoMS(cargoReportMovimentacaoMS, unitGKey);				
		} catch (FileNotFoundException | JAXBException | UnsupportedEncodingException e) {
			LOGGER.error("ERRO Processo Retorno  "+ e.getMessage());			
		}	
		return null;
	}
}
