package br.com.grupoLibra.boletimCarga.uteis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ManipulaArquivos {
	public static String gravaArquivo(String pathNomeArquivoExensao, String dados) throws IOException {
		File file = new File(pathNomeArquivoExensao);
		BufferedWriter output = new BufferedWriter(new FileWriter(file));
		output.write(dados);
		output.close();
		return file.getAbsolutePath();
	}
}
