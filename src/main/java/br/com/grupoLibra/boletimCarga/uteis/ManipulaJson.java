package br.com.grupoLibra.boletimCarga.uteis;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoResult;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportResult;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportResultUnit;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
public class ManipulaJson {
	
	public static String geraMensagemEnvioFila(String unitGkey, String pathArquivo) throws JsonProcessingException {		
		Map<String, String> objetoJson =  new HashMap<String, String>();
		objetoJson.put("unitGkey", unitGkey);
		objetoJson.put("pathArquivo", pathArquivo);	
		return new ObjectMapper().writeValueAsString(objetoJson);	
	}

	public static String geraCargoReport(CargoReportDTO cargoReportDTO) throws JsonProcessingException {		

		Map<String, String> objetoJson = new LinkedHashMap<String, String>();
       
		objetoJson.put("dateReturn",cargoReportDTO.getDateReturn() != null ? cargoReportDTO.getDateReturn().toString() : "");
		objetoJson.put("dateDispatch",cargoReportDTO.getDateDispatch() != null ? cargoReportDTO.getDateDispatch().toString() : "");
		objetoJson.put("dateReturn", cargoReportDTO.getDateReturn() != null ? cargoReportDTO.getDateReturn().toString() : "");
		objetoJson.put("facility",cargoReportDTO.getFacility() != null ? cargoReportDTO.getFacility().toString() : "");
		objetoJson.put("idPackageXML",cargoReportDTO.getIdPackageXML());
		objetoJson.put("status",cargoReportDTO.getStatus() != null ? cargoReportDTO.getStatus().toString() : "");
		objetoJson.put("terminalOperation",cargoReportDTO.getTerminalOperation());
		objetoJson.put("unitQuantity",cargoReportDTO.getUnitQuantity());
		objetoJson.put("escalaSiscomexCarga",cargoReportDTO.getEscalaSiscomexCarga());
		objetoJson.put("vesselVisitDetails", cargoReportDTO.getVesselVisitDetails() != null ? cargoReportDTO.getVesselVisitDetails().toString() : "");
		objetoJson.put("created",cargoReportDTO.getCreated() != null ? cargoReportDTO.getCreated().toString() : "");
		objetoJson.put("creator",cargoReportDTO.getCreator());
		objetoJson.put("gkey",cargoReportDTO.getGkey() != null ? cargoReportDTO.getGkey().toString() : "");
		
		return new ObjectMapper().writeValueAsString(objetoJson);	
	}

	public static String geraVesselVisit(CargoReportDTO cargoReportDTO) throws JsonProcessingException {		

		Map<String, String> objetoJson = new LinkedHashMap<String, String>();

		objetoJson.put("gkey",cargoReportDTO.getGkey().toString());
		objetoJson.put("vesselVisitDetails", cargoReportDTO.getVesselVisitDetails() != null ? cargoReportDTO.getVesselVisitDetails().toString() : "");
		
		return new ObjectMapper().writeValueAsString(objetoJson);	
	}

	public static String geraCargoReportResult(CargoReportDTO cargoReportDTO) throws JsonProcessingException {		
		Format formatter;
		formatter = new SimpleDateFormat("dd/MM/yy");
		String seals;
		Map<String, String> objetoJson = new LinkedHashMap<String, String>();

		objetoJson.put("boletimId",	cargoReportDTO.getIdPackageXML());
		objetoJson.put("vesselVisitId",	cargoReportDTO.getVesselVisitDetails() != null ?  cargoReportDTO.getVesselVisitDetails().toString() : "");
		objetoJson.put("escalaSiscomex", cargoReportDTO.getEscalaSiscomexCarga());
		objetoJson.put("dataEnvioFormatada", formatter.format(cargoReportDTO.getDateDispatch()));
		objetoJson.put("dataRetornoFormatada", formatter.format(cargoReportDTO.getDateReturn()));
		objetoJson.put("serpro", " ");
		objetoJson.put("qtdeItens",	cargoReportDTO.getUnitQuantity() != null ?  cargoReportDTO.getUnitQuantity().toString() : "");

		objetoJson.put("operacao", (cargoReportDTO.getCargoReportUnits() != null && cargoReportDTO.getCargoReportUnits().get(0) != null && cargoReportDTO.getCargoReportUnits().get(0).getTypeOperation() != null) ? cargoReportDTO.getCargoReportUnits().get(0).getTypeOperation().toString() : "");
		objetoJson.put("restow", (cargoReportDTO.getCargoReportUnits() != null && cargoReportDTO.getCargoReportUnits().get(0) != null) ?  cargoReportDTO.getCargoReportUnits().get(0).getFacilityUfvRestowType() : "");
		
		objetoJson.put("status", cargoReportDTO.getStatus());
		objetoJson.put("boletimGkey", cargoReportDTO.getGkey() != null ? cargoReportDTO.getGkey().toString() : "");

		for (CargoReportUnitDTO cargoReportDet : cargoReportDTO.getCargoReportUnits()) {

			objetoJson.put("unitBoletimGkey", cargoReportDet.getUnitGkey() != null ? cargoReportDet.getUnitGkey().toString() : "");

			seals = "";
			if (cargoReportDet.getSealNbr1() != null && !cargoReportDet.getSealNbr1().equals('-')) {
				seals = cargoReportDet.getSealNbr1().toString();
				}
			if (cargoReportDet.getSealNbr2() != null && !cargoReportDet.getSealNbr2().equals('-')) {
				seals = seals + " - " + cargoReportDet.getSealNbr2().toString();
			}
			if (cargoReportDet.getSealNbr3() != null && !cargoReportDet.getSealNbr3().equals('-')) {
				seals = seals + " - " + cargoReportDet.getSealNbr3().toString();
			}
			if (cargoReportDet.getSealNbr4() != null && !cargoReportDet.getSealNbr4().equals('-')) {
				seals = seals + " - " + cargoReportDet.getSealNbr4().toString();
			}
			objetoJson.put("seals", seals.toString());
			
			objetoJson.put("cargoQty", cargoReportDet.getCargoQuantity() );	
			objetoJson.put("freightKind", cargoReportDet.getFreightKind());	
			objetoJson.put("operacao", cargoReportDet.getTypeOperation());	
			objetoJson.put("manifesto",	cargoReportDet.getManifest());	
			objetoJson.put("ceMercante", cargoReportDet.getLading());	
			objetoJson.put("itemSiscomex", cargoReportDet.getNbrCargoItem());
			objetoJson.put("restow", cargoReportDet.getFacilityUfvRestowType());		
			objetoJson.put("damage", cargoReportDet.getIsDamage());		
			
			String status = "Erro";
			if (cargoReportDet.getItemStatus()) {
				status = "Sucesso";
			}
			
			objetoJson.put("status",status);
		}

		return new ObjectMapper().writeValueAsString(objetoJson);	
	}
	
	public static CargoReportResult geraCargoReportDTOParaCargoReportResult(CargoReportDTO cargoReportDTO) throws JsonProcessingException {		
		
		Format formatter;
		formatter = new SimpleDateFormat("dd/MM/yy");
		String seals;
		
		CargoReportResult cargoReportResult = new CargoReportResult();
		CargoReportResultUnit cargoReportResulUnit = new CargoReportResultUnit();
		List<CargoReportResultUnit> listaCargoReportResultUnit = new ArrayList<>();
		
		cargoReportResult.setBoletimId(cargoReportDTO.getIdPackageXML() != null ? cargoReportDTO.getIdPackageXML() : "");
		cargoReportResult.setVesselVisitId(cargoReportDTO.getVesselVisitDetails() != null ? cargoReportDTO.getVesselVisitDetails().toString() : "");
		cargoReportResult.setEscalaSiscomex(cargoReportDTO.getEscalaSiscomexCarga() != null ? cargoReportDTO.getEscalaSiscomexCarga() : "");
		cargoReportResult.setDataEnvioFormatada(cargoReportDTO.getDateDispatch() != null ? formatter.format(cargoReportDTO.getDateDispatch()) : "11/11/1111");
		cargoReportResult.setDataRetornoFormatada(cargoReportDTO.getDateReturn() != null ? formatter.format(cargoReportDTO.getDateReturn()) : "11/11/1111");
		cargoReportResult.setSerpro(" ");
		cargoReportResult.setQtdeItens(cargoReportDTO.getUnitQuantity() != null ? cargoReportDTO.getUnitQuantity().toString() : "");
		cargoReportResult.setOperacao(cargoReportDTO.getCargoReportUnits().get(0).getTypeOperation() != null ? cargoReportDTO.getCargoReportUnits().get(0).getTypeOperation() : "");
		cargoReportResult.setRestow(cargoReportDTO.getCargoReportUnits().get(0).getFacilityUfvRestowType() != null ? cargoReportDTO.getCargoReportUnits().get(0).getFacilityUfvRestowType() : "");
		cargoReportResult.setStatus(cargoReportDTO.getStatus()  != null ? cargoReportDTO.getStatus() : "");
		cargoReportResult.setBoletimGkey(cargoReportDTO.getGkey() != null ? cargoReportDTO.getGkey() : 0);
		
		for (CargoReportUnitDTO cargoReportUnitDTO : cargoReportDTO.getCargoReportUnits()) {
			
			cargoReportResulUnit.setUnitBoletimGkey(cargoReportUnitDTO.getUnitGkey());

			seals = "";
			if (cargoReportUnitDTO.getSealNbr1() != null && !cargoReportUnitDTO.getSealNbr1().equals('-')) {
				seals = cargoReportUnitDTO.getSealNbr1().toString();
				}
			if (cargoReportUnitDTO.getSealNbr2() != null && !cargoReportUnitDTO.getSealNbr2().equals('-')) {
				seals = seals + " - " + cargoReportUnitDTO.getSealNbr2().toString();
			}
			if (cargoReportUnitDTO.getSealNbr3() != null && !cargoReportUnitDTO.getSealNbr3().equals('-')) {
				seals = seals + " - " + cargoReportUnitDTO.getSealNbr3().toString();
			}
			if (cargoReportUnitDTO.getSealNbr4() != null && !cargoReportUnitDTO.getSealNbr4().equals('-')) {
				seals = seals + " - " + cargoReportUnitDTO.getSealNbr4().toString();
			}
			cargoReportResulUnit.setSeals(seals.toString());
			
			cargoReportResulUnit.setCargoQty(cargoReportUnitDTO.getCargoQuantity() != null ? cargoReportUnitDTO.getCargoQuantity() : "" );	
			cargoReportResulUnit.setFreightKind(cargoReportUnitDTO.getFreightKind() != null ? cargoReportUnitDTO.getFreightKind() : "");	
			cargoReportResulUnit.setOperacao(cargoReportUnitDTO.getTypeOperation() != null ? cargoReportUnitDTO.getTypeOperation() : "");	
			cargoReportResulUnit.setManifesto(cargoReportUnitDTO.getManifest() != null ? cargoReportUnitDTO.getManifest() : "");	
			cargoReportResulUnit.setCeMercante(cargoReportUnitDTO.getLading() != null ? cargoReportUnitDTO.getLading() : "");	
			cargoReportResulUnit.setItemSiscomex(cargoReportUnitDTO.getNbrCargoItem() != null ? cargoReportUnitDTO.getNbrCargoItem() : "");
			cargoReportResulUnit.setRestow(cargoReportUnitDTO.getFacilityUfvRestowType() != null ? cargoReportUnitDTO.getFacilityUfvRestowType() : "");		
			cargoReportResulUnit.setDamage(cargoReportUnitDTO.getIsDamage() != null ? cargoReportUnitDTO.getIsDamage() : "");		
			
			String status = "Erro";
			if (cargoReportUnitDTO.getItemStatus() != null) {
				if ((cargoReportUnitDTO.getItemStatus()) && (cargoReportUnitDTO.getItemStatus() != null)) {
					status = "Sucesso";
			}}
			
			cargoReportResulUnit.setStatus(status);
			
			listaCargoReportResultUnit.add(cargoReportResulUnit);
		}
	    
	 	cargoReportResult.setCargoReportResultUnit(listaCargoReportResultUnit);
	 	
		return cargoReportResult; 
	}

	public static CargoReportResult geraCargoReportUnitDTOParaCargoReportResult(CargoReportUnitDTO cargoReportUnitDTO) throws JsonProcessingException {		
		
		Format formatter;
		formatter = new SimpleDateFormat("dd/MM/yy");
		String seals;
		
		CargoReportResult cargoReportResult = new CargoReportResult();
		CargoReportResultUnit cargoReportResulUnit = new CargoReportResultUnit();
		List<CargoReportResultUnit> listaCargoReportResultUnit = new ArrayList<>();
		 
		cargoReportResult.setBoletimId(cargoReportUnitDTO.getCargoReportDTO().getIdPackageXML() != null ? cargoReportUnitDTO.getCargoReportDTO().getIdPackageXML() : "" );
		cargoReportResult.setVesselVisitId(cargoReportUnitDTO.getCargoReportDTO().getVesselVisitDetails() != null ? cargoReportUnitDTO.getCargoReportDTO().getVesselVisitDetails().toString() : "");
		cargoReportResult.setEscalaSiscomex(cargoReportUnitDTO.getCargoReportDTO().getEscalaSiscomexCarga() != null ? cargoReportUnitDTO.getCargoReportDTO().getEscalaSiscomexCarga() : "");
		cargoReportResult.setDataEnvioFormatada(cargoReportUnitDTO.getCargoReportDTO().getDateDispatch() != null ? formatter.format(cargoReportUnitDTO.getCargoReportDTO().getDateDispatch()) : "");
		cargoReportResult.setDataRetornoFormatada(cargoReportUnitDTO.getCargoReportDTO().getDateReturn() != null ? formatter.format(cargoReportUnitDTO.getCargoReportDTO().getDateReturn()) : "");
		cargoReportResult.setSerpro(" ");
		cargoReportResult.setQtdeItens(cargoReportUnitDTO.getCargoReportDTO().getUnitQuantity() != null ? cargoReportUnitDTO.getCargoReportDTO().getUnitQuantity().toString() : "");
		cargoReportResult.setOperacao(cargoReportUnitDTO.getTypeOperation() != null ? cargoReportUnitDTO.getTypeOperation().toString() : "");
		cargoReportResult.setRestow(cargoReportUnitDTO.getFacilityUfvRestowType() != null ?  cargoReportUnitDTO.getFacilityUfvRestowType() : "");
		cargoReportResult.setStatus(cargoReportUnitDTO.getCargoReportDTO().getStatus() != null ? cargoReportUnitDTO.getCargoReportDTO().getStatus() : "");
		cargoReportResult.setBoletimGkey(cargoReportUnitDTO.getCargoReportDTO().getGkey() != null ? cargoReportUnitDTO.getCargoReportDTO().getGkey() : 0);
		
		cargoReportResulUnit.setUnitBoletimGkey(cargoReportUnitDTO.getUnitGkey() != null ? cargoReportUnitDTO.getUnitGkey() : 0);

		seals = "";
		if (cargoReportUnitDTO.getSealNbr1() != null && !cargoReportUnitDTO.getSealNbr1().equals('-')) {
			seals = cargoReportUnitDTO.getSealNbr1().toString();
			}
		if (cargoReportUnitDTO.getSealNbr2() != null && !cargoReportUnitDTO.getSealNbr2().equals('-')) {
			seals = seals + " - " + cargoReportUnitDTO.getSealNbr2().toString();
		}
		if (cargoReportUnitDTO.getSealNbr3() != null && !cargoReportUnitDTO.getSealNbr3().equals('-')) {
			seals = seals + " - " + cargoReportUnitDTO.getSealNbr3().toString();
		}
		if (cargoReportUnitDTO.getSealNbr4() != null && !cargoReportUnitDTO.getSealNbr4().equals('-')) {
			seals = seals + " - " + cargoReportUnitDTO.getSealNbr4().toString();
		}
		cargoReportResulUnit.setSeals(seals.toString());
		
		cargoReportResulUnit.setCargoQty(cargoReportUnitDTO.getCargoQuantity() != null ? cargoReportUnitDTO.getCargoQuantity() : "");	
		cargoReportResulUnit.setFreightKind(cargoReportUnitDTO.getFreightKind() != null ? cargoReportUnitDTO.getFreightKind() : "");	
		cargoReportResulUnit.setOperacao(cargoReportUnitDTO.getTypeOperation() != null ? cargoReportUnitDTO.getTypeOperation() : "");	
		cargoReportResulUnit.setManifesto(cargoReportUnitDTO.getManifest() != null ? cargoReportUnitDTO.getManifest() : "");	
		cargoReportResulUnit.setCeMercante(cargoReportUnitDTO.getLading() != null ? cargoReportUnitDTO.getLading() : "");	
		cargoReportResulUnit.setItemSiscomex(cargoReportUnitDTO.getNbrCargoItem() != null ? cargoReportUnitDTO.getNbrCargoItem() : "");
		cargoReportResulUnit.setRestow(cargoReportUnitDTO.getFacilityUfvRestowType() != null ? cargoReportUnitDTO.getFacilityUfvRestowType() : "");		
		cargoReportResulUnit.setDamage(cargoReportUnitDTO.getIsDamage() != null ? cargoReportUnitDTO.getIsDamage() : "");		
		
		String status = "Erro";
		if (cargoReportUnitDTO.getItemStatus() == null) {
			status = "";
		} else if (cargoReportUnitDTO.getItemStatus()) {
		   status = "Sucesso";
		}
			
		cargoReportResulUnit.setStatus(status);
			
		listaCargoReportResultUnit.add(cargoReportResulUnit);
	    
		cargoReportResult.setCargoReportResultUnit(listaCargoReportResultUnit);
	 	
	return cargoReportResult; 
	}

	public static CargoReportMovimentacaoResult geraCargoReportMovimentacao(CargoReportMovimentacaoMS cargoReportMovimentacao,String tipo) throws JsonProcessingException {		

		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String dataFormatada;
		Date data = null ;
		
		CargoReportMovimentacaoResult cargoReportMovimentacaoResult = new CargoReportMovimentacaoResult();

		cargoReportMovimentacaoResult.setVesselVisitDetails(cargoReportMovimentacao.getCargoReportDTO().getVesselVisitDetails() != null ? cargoReportMovimentacao.getCargoReportDTO().getVesselVisitDetails() : 0);
		cargoReportMovimentacaoResult.setId(cargoReportMovimentacao.getCargoReportDTO().getGkey() != null ? cargoReportMovimentacao.getCargoReportDTO().getGkey() : 0);
		
		if (cargoReportMovimentacao.getDtOcorrencia() != null) {
			try {
				dataFormatada = cargoReportMovimentacao.getDtOcorrencia().toString();
				data = formato.parse(dataFormatada);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		cargoReportMovimentacaoResult.setDtOcorrencia(data);
		cargoReportMovimentacaoResult.setTipoMovimentacao(tipo != null ? tipo : "");
		cargoReportMovimentacaoResult.setStatus(cargoReportMovimentacao.getStatus() != null ? cargoReportMovimentacao.getStatus() : "");
		cargoReportMovimentacaoResult.setCodigo(cargoReportMovimentacao.getCodigo() != null ? cargoReportMovimentacao.getCodigo() : "");
		cargoReportMovimentacaoResult.setDescricao(cargoReportMovimentacao.getDescricao() != null ? cargoReportMovimentacao.getDescricao() : "");
		
		return cargoReportMovimentacaoResult;	
	}

}
