package br.com.grupoLibra.boletimCarga.uteis;

public class ManipulaObject {
	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEmpty(String s) {
		if (s == null) {
			return true;
		} else if (s.trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEmpty(Object[] arr) {
		if (arr == null) {
			return true;
		} else if (arr.length == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEmpty(long[] arr) {
		if (arr == null) {
			return true;
		} else if (arr.length == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEmpty(long l) {
		return l == 0;
	}

	public static boolean isEmpty(Long l) {
		return l == null ? true : l == 0;
	}

	public static boolean isEmpty(Integer l) {
		return l == null ? true : l == 0;
	}
}
