package br.com.grupoLibra.boletimCarga.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.n4.repositories.ConsultasN4;

@Service("BuscaDadosN4Service")
public class BuscaDadosN4Service implements IService{

	@Autowired
	ConsultasN4 consultasN4;
	
	@Override
	public String execute(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}

	public CargoReportDTO execute(String gKey) {
		return consultasN4.buscaUnit(gKey);		
	}

	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}
}
