package br.com.grupoLibra.boletimCarga.services;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.w3._2000._09.xmldsig_.SignatureType;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO.Cargassoltas;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO.Conteineres;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaSoltaWSVO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.ConteinerWSVO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.ConteinerWSVO.NusLacresConteiner;


@Service("PreparaCargaDescargaWSVOService")
public class PreparaCargaDescargaWSVOService implements IService{

	@Override
	public String execute(String localArquivo, String unitGKey) {
		return null;
	}

	public CargaDescargaWSVO execute(CargoReportDTO cargoReportDTO) {
		//ObjectFactory objectFactory = new ObjectFactory(); 
		CargaDescargaWSVO cargaDesargaWSVO = new CargaDescargaWSVO();
				//objectFactory.createCargaDescargaWSVO();

		cargaDesargaWSVO.setCdTerminalOperacao(cargoReportDTO.getTerminalOperation());
		cargaDesargaWSVO.setNrEscala(cargoReportDTO.getEscalaSiscomexCarga());
		Integer tipoItem = Integer.parseInt(cargoReportDTO.getCargoReportUnits().get(0).getTypeItem());
		cargaDesargaWSVO.setCdTipoItem(tipoItem);

		if (tipoItem.equals(2)) {
			CargaSoltaWSVO cargaSolta;
			Cargassoltas cargassoltas = new CargaDescargaWSVO.Cargassoltas();
			for (CargoReportUnitDTO cargoReportUnitDTO : cargoReportDTO.getCargoReportUnits()) {
				cargaSolta = new CargaSoltaWSVO();

				cargaSolta.setCdOperacao(cargoReportUnitDTO.getTypeOperation());
				cargaSolta.setCdTipoMovimentacao(cargoReportUnitDTO.getTypeMovimentation());

				if (cargoReportUnitDTO.getIsDamage().equals("S")) {
					cargaSolta.setInAvariaOcorrencia(cargoReportUnitDTO.getIsDamage());
				} else {
					cargaSolta.setInAvariaOcorrencia("N");
				}

				cargaSolta.setQtOperacao(new BigDecimal(cargoReportUnitDTO.getCargoQuantity()));

				if (cargaSolta.getCdTipoMovimentacao().equals("00")) {
					cargaSolta.setNrItem(cargoReportUnitDTO.getNbrCargoItem());
					cargaSolta.setNrConhecimento(cargoReportUnitDTO.getLading());
					cargaSolta.setNrManifesto(cargoReportUnitDTO.getManifest());
				}

				cargassoltas.getCargasolta().add(cargaSolta);
			}

			cargaDesargaWSVO.setCargassoltas(cargassoltas);

		} else {
			ConteinerWSVO conteiner;
			
			Conteineres conteinerLista = new CargaDescargaWSVO.Conteineres();

			for (CargoReportUnitDTO cargoReportUnitDTO : cargoReportDTO.getCargoReportUnits()) {				
				conteiner = new ConteinerWSVO();

				conteiner.setCdOperacao(cargoReportUnitDTO.getTypeOperation());
				conteiner.setCdTipoMovimentacao(cargoReportUnitDTO.getTypeMovimentation());

				if (cargoReportUnitDTO.getIsDamage().equals("S")) {
					conteiner.setInAvariaOcorrencia(cargoReportUnitDTO.getIsDamage());
					NusLacresConteiner lacres = new NusLacresConteiner();
					lacres.getNuLacreConteiner().add(cargoReportUnitDTO.getSealNbr1());
					lacres.getNuLacreConteiner().add(cargoReportUnitDTO.getSealNbr2());
					lacres.getNuLacreConteiner().add(cargoReportUnitDTO.getSealNbr3());
					lacres.getNuLacreConteiner().add(cargoReportUnitDTO.getSealNbr4());
					conteiner.setNusLacresConteiner(lacres);
				} else {
					conteiner.setInAvariaOcorrencia("N");
				}

				conteiner.setNrConteiner(cargoReportUnitDTO.getUnitNbr());

				if (conteiner.getCdTipoMovimentacao().equals("00")) {
					conteiner.setNrManifesto(cargoReportUnitDTO.getManifest());
				}

				if (!cargoReportUnitDTO.getFreightKind().equals("MTY")) {
					if (conteiner.getCdTipoMovimentacao().equals("00")) {
						conteiner.setNrConhecimento(cargoReportUnitDTO.getLading());//
						conteiner.setNrItem(cargoReportUnitDTO.getNbrCargoItem());
					}
				}				
				conteinerLista.getConteiner().add(conteiner);				
			}
			cargaDesargaWSVO.setConteineres(conteinerLista);
			
		}
		
		SignatureType signature = new SignatureType();
		cargaDesargaWSVO.setSignature(signature);
		return cargaDesargaWSVO;
	}


	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
