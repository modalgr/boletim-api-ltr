package br.com.grupoLibra.boletimCarga.services;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.repositories.CargoReportDTORepository;

@Service("PersisteDadosBDService")
public class PersisteDadosBDService {
	
	@Autowired
	private CargoReportDTORepository cargoReportDTORepository;
	
	@Autowired
	private ExtraiDadosXmlService extraiDadosXmlService;
	
	@Transactional
	public void gravaCargoReportMovimentacaoMS(String unitGkey, String statusMS, String pathArquivo) throws ParserConfigurationException, SAXException, IOException {
		CargoReportDTO cargoReportDTO = cargoReportDTORepository.findOne(Long.parseLong(unitGkey));
		if (cargoReportDTO == null) {
			cargoReportDTO =extraiDadosXmlService.execute(
					extraiDadosXmlService.execute(pathArquivo), unitGkey);
			cargoReportDTO.setStatus(CargoReportDTO.STATUS_INTEGRADO_XML);
			
			CargoReportMovimentacaoMS cargoReportMovimentacaoMS = new CargoReportMovimentacaoMS();
			
			cargoReportMovimentacaoMS.setStatus(CargoReportMovimentacaoMS.STATUSMS_INTEGRADO);
			cargoReportMovimentacaoMS.setDtOcorrencia(new DateTime().toDate());	
			cargoReportMovimentacaoMS.setCargoReportDTO(cargoReportDTO);
			cargoReportDTO.getCargoReportMovimentacaoMS().add(cargoReportMovimentacaoMS);			
			
			cargoReportDTO = cargoReportDTORepository.saveAndFlush(cargoReportDTO);
		}
		gravaCargoReportMovimentacaoMS(cargoReportDTO, statusMS, pathArquivo);
	}
	
	@Transactional
	public void gravaCargoReportMovimentacaoMS(CargoReportDTO cargoReportDTO, String statusMS, String pathArquivo) {
		
		
		CargoReportMovimentacaoMS cargoReportMovimentacaoMS = new CargoReportMovimentacaoMS();
		cargoReportMovimentacaoMS.setCargoReportDTO(cargoReportDTO);
		if (pathArquivo != null) {
			cargoReportMovimentacaoMS.setPathArquivo(pathArquivo);
		}		
		cargoReportMovimentacaoMS.setStatus(statusMS);
		cargoReportMovimentacaoMS.setDtOcorrencia(new DateTime().toDate());		
		cargoReportDTO.getCargoReportMovimentacaoMS().add(cargoReportMovimentacaoMS);
		cargoReportDTORepository.saveAndFlush(cargoReportDTO);		

		
	}
	
	@Transactional
	public void gravaCargoReportMovimentacaoMS(CargoReportMovimentacaoMS cargoReportMovimentacaoMS, String unitGkey) {
		CargoReportDTO cargoReportDTO = cargoReportDTORepository.findOne(Long.parseLong(unitGkey));
		cargoReportMovimentacaoMS.setCargoReportDTO(cargoReportDTO);
		cargoReportDTO.getCargoReportMovimentacaoMS().add(cargoReportMovimentacaoMS);		
		cargoReportDTORepository.saveAndFlush(cargoReportDTO);
	}
	
	@Transactional
	public void gravaCargoReportDTO(CargoReportDTO cargoReportDTO, String statusMS) {
		CargoReportMovimentacaoMS cargoReportMovimentacaoMS = new CargoReportMovimentacaoMS();
		cargoReportMovimentacaoMS.setStatus(statusMS);
		cargoReportMovimentacaoMS.setDtOcorrencia(new DateTime().toDate());
		cargoReportMovimentacaoMS.setCargoReportDTO(cargoReportDTO);
		cargoReportDTO.getCargoReportMovimentacaoMS().add(cargoReportMovimentacaoMS);
		cargoReportDTORepository.saveAndFlush(cargoReportDTO);
	
	}

}
