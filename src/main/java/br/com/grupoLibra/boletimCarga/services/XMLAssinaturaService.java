package br.com.grupoLibra.boletimCarga.services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.DigestMethodParameterSpec;
import javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import br.com.grupoLibra.boletimCarga.configuration.ArquivoConfiguracao;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaDatas;

@Service("XMLAssinaturaService")
public class XMLAssinaturaService implements IService {
	
	@Override
	public String execute(String localArquivo, String unitGKey) {		
		return null;
	}
	
	// retorna o path do arquivo assinado
	public String execute(X509Certificate x509Certificate, PrivateKey privateKey,
			String pathRecebeuFila, String gKey) throws NoSuchAlgorithmException, SAXException, ParserConfigurationException,
			MarshalException, XMLSignatureException, TransformerException, IOException {
		String arquivoXMLBytes = null;
		File arquivoXml = null;
		FileOutputStream arquivoXMLStreanAssinado = null;

		arquivoXml = new File(pathRecebeuFila);
		arquivoXMLBytes = addLinhaXML(pathRecebeuFila);

		XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
		Reference reference = null;
		SignedInfo signedInfo = null;

		try {
			reference = xmlSignatureFactory.newReference("",
					xmlSignatureFactory.newDigestMethod("http://www.w3.org/2000/09/xmldsig#sha1",
							(DigestMethodParameterSpec) null),
					Collections.singletonList(xmlSignatureFactory.newTransform(
							"http://www.w3.org/2000/09/xmldsig#enveloped-signature", (TransformParameterSpec) null)),
					(String) null, (String) null);
			signedInfo = xmlSignatureFactory.newSignedInfo(
					xmlSignatureFactory.newCanonicalizationMethod("http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
							(C14NMethodParameterSpec) null),
					xmlSignatureFactory.newSignatureMethod("http://www.w3.org/2000/09/xmldsig#rsa-sha1",
							(SignatureMethodParameterSpec) null),
					Collections.singletonList(reference));
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		KeyInfoFactory keyInfoFactory = xmlSignatureFactory.getKeyInfoFactory();
		ArrayList list509Content = new ArrayList();
		list509Content.add(x509Certificate.getSubjectX500Principal().getName());
		list509Content.add(x509Certificate);
		X509Data x509Data = keyInfoFactory.newX509Data(list509Content);
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));
		Document document = null;

		document = documentFactory(arquivoXMLBytes);

		DOMSignContext domSignContext = new DOMSignContext(privateKey, document.getDocumentElement());
		XMLSignature signature = xmlSignatureFactory.newXMLSignature(signedInfo, keyInfo);

		signature.sign(domSignContext);
		String nomePathNomeExtArqAssinado=ArquivoConfiguracao.getPathArquivoAssinados()+ ManipulaDatas.getCompNomeArquivo() + gKey+".xml";
		arquivoXMLStreanAssinado = new FileOutputStream(nomePathNomeExtArqAssinado);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		transformer = transformerFactory.newTransformer();
		transformer.transform(new DOMSource(document), new StreamResult(arquivoXMLStreanAssinado));
		System.out.println(new Date() + " - " + "Arquivo assinado criado com sucesso. "+nomePathNomeExtArqAssinado);	
		
		return nomePathNomeExtArqAssinado;
	}

	private String addLinhaXML(String pathFileXML) throws IOException {
		String linha = "";
		StringBuilder xml = new StringBuilder();

		BufferedReader in;

		in = new BufferedReader(new InputStreamReader(new FileInputStream(pathFileXML)));
		while ((linha = in.readLine()) != null) {
			xml.append(linha);
		}
		in.close();

		return xml.toString();
	}

	private Document documentFactory(String xml) throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document document = null;

		document = factory.newDocumentBuilder().parse(new ByteArrayInputStream(xml.getBytes()));

		return document;
	}

	@Deprecated
	public KeyInfo getkey(X509Certificate x509Certificate) {
		XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
		Reference reference = null;
		SignedInfo signedInfo = null;

		try {
			reference = xmlSignatureFactory.newReference("",
					xmlSignatureFactory.newDigestMethod("http://www.w3.org/2000/09/xmldsig#sha1",
							(DigestMethodParameterSpec) null),
					Collections.singletonList(xmlSignatureFactory.newTransform(
							"http://www.w3.org/2000/09/xmldsig#enveloped-signature", (TransformParameterSpec) null)),
					(String) null, (String) null);
			signedInfo = xmlSignatureFactory.newSignedInfo(
					xmlSignatureFactory.newCanonicalizationMethod("http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
							(C14NMethodParameterSpec) null),
					xmlSignatureFactory.newSignatureMethod("http://www.w3.org/2000/09/xmldsig#rsa-sha1",
							(SignatureMethodParameterSpec) null),
					Collections.singletonList(reference));
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e1) {
			System.out.println("Erro Criação factore" + e1.getMessage());
			e1.printStackTrace();
		}

		KeyInfoFactory keyInfoFactory = xmlSignatureFactory.getKeyInfoFactory();
		ArrayList list509Content = new ArrayList();
		list509Content.add(x509Certificate.getSubjectX500Principal().getName());
		list509Content.add(x509Certificate);
		X509Data x509Data = keyInfoFactory.newX509Data(list509Content);
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));

		return keyInfo;
	}

	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {
		return null;
	}

}
