package br.com.grupoLibra.boletimCarga.services;

public class FactoryService {
	
	public static IService createXMLService() {
		return new XMLService();
	}
	
	public static IService createRegrasService() {
		return new RegrasService();
	}
	
	public static IService createPreparaCargaDescargaWSVOService() {
		return new PreparaCargaDescargaWSVOService();
	}

	public static IService createExtraiDadosXmlService() {
		return new ExtraiDadosXmlService();
	}
	
	public static IService createBuscaDadosN4Service() {
		return new BuscaDadosN4Service();
	}	
	
	public static IService createXMLAssinaturaService() {
		return new XMLAssinaturaService();
	}
	
}
