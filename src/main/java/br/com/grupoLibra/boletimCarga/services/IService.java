package br.com.grupoLibra.boletimCarga.services;

public interface IService {
	String execute(String localArquivo, String unitGKey);
	Boolean verifique(String localArquivo, String unitGKey);
}
