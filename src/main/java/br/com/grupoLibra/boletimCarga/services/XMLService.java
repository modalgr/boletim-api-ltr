package br.com.grupoLibra.boletimCarga.services;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaArquivos;
import br.com.grupoLibra.boletimCarga.uteis.ManipulaDatas;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO;

@Service("XMLService")
public class XMLService implements IService{
	
	@Override
	public String execute(String localArquivo, String unitGKey) {		
		return null;
	}
	
	public String execute(CargoReportDTO cargoReportDTO, CargaDescargaWSVO cargaDescargaWSVO, String Path) {		
		String nomeArquivo =  ManipulaDatas.getCompNomeArquivo()+cargoReportDTO.getGkey();		
		return execute(nomeArquivo,cargaDescargaWSVO, Path);		
	}
	
	public String execute(String nomeArquivo, CargaDescargaWSVO cargaDescargaWSVO, String Path) {
		try {
			XStream xstream = new XStream(new StaxDriver());
			return ManipulaArquivos.gravaArquivo(Path+nomeArquivo + ".xml", xstream.toXML(cargaDescargaWSVO));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}
}
