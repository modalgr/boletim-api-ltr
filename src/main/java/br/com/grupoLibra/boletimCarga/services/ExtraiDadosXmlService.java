package br.com.grupoLibra.boletimCarga.services;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Service;
import org.w3._2000._09.xmldsig_.CanonicalizationMethodType;
import org.w3._2000._09.xmldsig_.DigestMethodType;
import org.w3._2000._09.xmldsig_.KeyInfoType;
import org.w3._2000._09.xmldsig_.ReferenceType;
import org.w3._2000._09.xmldsig_.SignatureMethodType;
import org.w3._2000._09.xmldsig_.SignatureType;
import org.w3._2000._09.xmldsig_.SignedInfoType;
import org.w3._2000._09.xmldsig_.TransformType;
import org.w3._2000._09.xmldsig_.TransformsType;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportMovimentacaoMS;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.CargaDescargaWSVO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.ConteinerWSVO;
import br.gov.serpro.siscomex.carga.webservice.cargadescarga.ObjectFactory;

@Service("ExtraiDadosXmlService")
public class ExtraiDadosXmlService implements IService{
	
	@Override
	public String execute(String localArquivo, String unitGKey) {		
		return null;
	}

	public CargoReportDTO execute(CargaDescargaWSVO cargaDescargaWSVO, String unitGkey ) {		
		CargoReportDTO cargoReportDTO = new CargoReportDTO();
		cargoReportDTO.setGkey(Long.parseLong(unitGkey));
		cargoReportDTO.setEscalaSiscomexCarga(cargaDescargaWSVO.getNrEscala());
		cargoReportDTO.setTerminalOperation(cargaDescargaWSVO.getCdTerminalOperacao());
		
		List<CargoReportUnitDTO> cargoReportUnitDTOs = new LinkedList<CargoReportUnitDTO>();
		List<CargoReportMovimentacaoMS> cargoReportMovimentacaoMSs = new LinkedList<CargoReportMovimentacaoMS>();		 
		
		for (ConteinerWSVO conteinerWSVO : cargaDescargaWSVO.getConteineres().getConteiner()) {
			CargoReportUnitDTO cargoReportUnitDTO = new CargoReportUnitDTO();
			cargoReportUnitDTO.setTypeMovimentation( String.valueOf(cargaDescargaWSVO.getCdTipoItem()) );
			cargoReportUnitDTO.setTypeOperation(conteinerWSVO.getCdOperacao());
			cargoReportUnitDTO.setTypeMovimentation(conteinerWSVO.getCdTipoMovimentacao());
			cargoReportUnitDTO.setIsDamage(conteinerWSVO.getInAvariaOcorrencia());
			cargoReportUnitDTO.setManifest(conteinerWSVO.getNrManifesto());
			cargoReportUnitDTO.setLading(conteinerWSVO.getNrConhecimento());
			cargoReportUnitDTO.setNbrCargoItem(conteinerWSVO.getNrItem());
			cargoReportUnitDTO.setUnitNbr(conteinerWSVO.getNrConteiner());
			cargoReportUnitDTO.setCargoReportDTO(cargoReportDTO);
			cargoReportUnitDTO.setTypeItem("1");
			cargoReportUnitDTOs.add(cargoReportUnitDTO);
		}
		
		cargoReportDTO.setCargoReportUnits(cargoReportUnitDTOs);
		cargoReportDTO.setCargoReportMovimentacaoMS(cargoReportMovimentacaoMSs);		
		
		return cargoReportDTO;
	}
	
	public CargaDescargaWSVO execute(String arquivoXMLAssinado) throws ParserConfigurationException, SAXException, IOException {
		ObjectFactory factory = new ObjectFactory();
		CargaDescargaWSVO cargaDescargaWSVO = factory.createCargaDescargaWSVO();

		CargaDescargaWSVO.Conteineres conteiners = factory.createCargaDescargaWSVOConteineres();

		// fazer o parse do arquivo e criar o documento XML
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(arquivoXMLAssinado);
		// elemento raiz
		// org.w3c.dom.Element raiz = document.getDocumentElement();
		// elementos filhos
		NodeList dadosLoteNodeList = document.getElementsByTagName("ns3:cargaDescargaWSVO");

		for (int i = 0; i < dadosLoteNodeList.getLength(); i++) {
			NodeList dadosLoteChildrenNodeList = dadosLoteNodeList.item(i).getChildNodes();
			for (int j = 0; j < dadosLoteChildrenNodeList.getLength(); j++) {
				Node node = dadosLoteChildrenNodeList.item(j);

				if (node == null || node.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				if (node.getNodeName().equals("nrEscala")) {
					cargaDescargaWSVO.setNrEscala(node.getTextContent());
				} else if (node.getNodeName().equals("cdTerminalOperacao")) {
					cargaDescargaWSVO.setCdTerminalOperacao(node.getTextContent());
				} else if (node.getNodeName().equals("cdTipoItem")) {
					cargaDescargaWSVO.setCdTipoItem(Integer.parseInt(node.getTextContent()));
				} else if (node.getNodeName().equals("conteineres")) {
					Node childNode = node.getFirstChild();
					childNode = childNode.getNextSibling();
					if (childNode == null || childNode.getNodeType() != childNode.ELEMENT_NODE) {
						continue;
					}
					
					if (childNode.getNodeName().equals("conteiner")) {
						Node childNode2 = childNode.getFirstChild();
						ConteinerWSVO conteiner = factory.createConteinerWSVO();
						while (childNode2.getNextSibling() != null) {
							childNode2 = childNode2.getNextSibling();
							if (!(childNode2 == null || childNode2.getNodeType() != childNode2.ELEMENT_NODE)) {

								if (childNode2.getNodeName().equals("cdOperacao")) {
									conteiner.setCdOperacao(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("cdTipoMovimentacao")) {
									conteiner.setCdTipoMovimentacao(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("inAvariaOcorrencia")) {
									conteiner.setInAvariaOcorrencia(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("nrManifesto")) {
									conteiner.setNrManifesto(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("nrConhecimento")) {
									conteiner.setNrConhecimento(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("nrItem")) {
									conteiner.setNrItem(childNode2.getTextContent());
								}
								if (childNode2.getNodeName().equals("nrConteiner")) {
									conteiner.setNrConteiner(childNode2.getTextContent());
								}
							}
						}
						conteiners.getConteiner().add(conteiner);
					}
				}
			}
		}
		cargaDescargaWSVO.setConteineres(conteiners);
		return cargaDescargaWSVO;
	}
	
	public CargaDescargaWSVO execute(String arquivoXMLAssinado, KeyInfo keyInfo)
			throws ParserConfigurationException, IOException, SAXException {
		CargaDescargaWSVO cargaDescargaWSVO = execute(arquivoXMLAssinado);
		// ---signedInfo
		SignatureType signatureType = new SignatureType();
		SignedInfoType signedInfoType = new SignedInfoType();
		CanonicalizationMethodType canonicalizationMethodType = new CanonicalizationMethodType();
		SignatureMethodType signatureMethodType = new SignatureMethodType();
		// List<ReferenceType> reference = new ArrayList<>();
		ReferenceType referenceType = new ReferenceType();
		TransformsType transformsType = new TransformsType();
		TransformType transformType = new TransformType();
		DigestMethodType digestMethodType = new DigestMethodType();
		//SignatureValueType signatureValueType = new SignatureValueType();

		// SignedInfo
		canonicalizationMethodType.setAlgorithm("http://www.w3.org/TR/2001/REC-xml-c14n-20010315");
		signedInfoType.setCanonicalizationMethod(canonicalizationMethodType);

		signatureMethodType.setAlgorithm("http://www.w3.org/2000/09/xmldsig#sha1");
		signedInfoType.setSignatureMethod(signatureMethodType);

		// reference
		transformType.setAlgorithm("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
		transformsType.getTransform().add(transformType);
		referenceType.setTransforms(transformsType);

		digestMethodType.setAlgorithm("http://www.w3.org/2000/09/xmldsig#rsa-sha1");
		referenceType.setDigestMethod(digestMethodType);

		KeyInfoType KeyInfoType = new KeyInfoType();
		KeyInfoType.getContent().add(keyInfo);
		signatureType.setKeyInfo(KeyInfoType);

		signatureType.setSignedInfo(signedInfoType);
		cargaDescargaWSVO.setSignature(signatureType);
		return cargaDescargaWSVO;
	}

	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}
}
