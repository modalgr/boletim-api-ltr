package br.com.grupoLibra.boletimCarga.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.grupoLibra.boletimCarga.api.models.CargoReportDTO;
import br.com.grupoLibra.boletimCarga.api.models.CargoReportUnitDTO;
import br.com.grupoLibra.boletimCarga.n4.repositories.ConsultasN4;

@Service("RegrasService")
public class RegrasService implements IService {
	@Autowired
	ConsultasN4 consultaN4;

	@Override
	public Boolean verifique(String localArquivo, String unitGKey) {		
		return null;
	}

	public Boolean verifique(CargoReportDTO cargoReportDTO) {
		CargoReportUnitDTO cargoReportUnitDTO = new CargoReportUnitDTO();
		for (CargoReportUnitDTO unit : cargoReportDTO.getCargoReportUnits()) {
			cargoReportUnitDTO = unit;
		}
		
		boolean validateSeal =  consultaN4.validateSeal(cargoReportUnitDTO.getPrimaryUe());

		if (cargoReportDTO.getEscalaSiscomexCarga() == null || cargoReportDTO.getEscalaSiscomexCarga().isEmpty()) {
			return false;
		}

		if (cargoReportDTO.getTerminalOperation() == null || cargoReportDTO.getTerminalOperation().isEmpty()) {
			return false;
		}

		if (cargoReportUnitDTO.getTypeItem() == null || cargoReportUnitDTO.getTypeItem().isEmpty()) {
			return false;
		}
		// inAvariasOcorrencia
		if (cargoReportUnitDTO.getIsDamage() == null || cargoReportUnitDTO.getIsDamage().isEmpty()) {
			return false;
		}

		if (cargoReportUnitDTO.getTypeOperation() == null || cargoReportUnitDTO.getTypeOperation().isEmpty()) {
			return false;
		}

		if (!"00".equals(cargoReportUnitDTO.getTypeMovimentation())
				&& !"01".equals(cargoReportUnitDTO.getTypeMovimentation())) {
			return false;
		}

		if ("FCL".equals(cargoReportUnitDTO.getFreightKind()) || "LCL".equals(cargoReportUnitDTO.getFreightKind())) {
			if (cargoReportUnitDTO.getUnitNbr() == null || cargoReportUnitDTO.getUnitNbr().isEmpty()) {
				return false;
			}

			//validateSeal = consultaN4.validateSeal(cargoReportUnitDTO.getUnitGkey());

			if ("NONE".equals(cargoReportUnitDTO.getFacilityUfvRestowType())) {
				if (cargoReportUnitDTO.getUnitFlexString15() == null
						|| cargoReportUnitDTO.getUnitFlexString15().isEmpty()) {
					return false;
				}

				if (cargoReportUnitDTO.getUnitFlexString13() == null
						|| cargoReportUnitDTO.getUnitFlexString13().isEmpty()) {
					return false;
				}

				String nrItem = cargoReportUnitDTO.getUnitFlexString14();

				if (nrItem == null || nrItem.isEmpty()) {
					return false;
				}
			}
		} else if ("MTY".equals(cargoReportUnitDTO.getFreightKind())) {
			validateSeal = consultaN4.validateSeal(cargoReportUnitDTO.getUnitGkey());

			if (cargoReportUnitDTO.getUnitNbr() == null || cargoReportUnitDTO.getUnitNbr().isEmpty()) {
				return false;
			}

			if ("NONE".equals(cargoReportUnitDTO.getFacilityUfvRestowType())) {
				if (cargoReportUnitDTO.getUnitFlexString15() == null
						|| cargoReportUnitDTO.getUnitFlexString15().isEmpty()) {
					return false;
				}
			}
		} else if ("BBK".equals(cargoReportUnitDTO.getFreightKind())) {
			if (cargoReportUnitDTO.getUnitCargoQuantity() == null
					|| cargoReportUnitDTO.getUnitCargoQuantity().isNaN()) {
				return false;
			}

			if ("NONE".equals(cargoReportUnitDTO.getFacilityUfvRestowType())) {
				if (cargoReportUnitDTO.getUnitFlexString15() == null
						|| cargoReportUnitDTO.getUnitFlexString15().isEmpty()) {
					return false;
				}

				if (cargoReportUnitDTO.getUnitFlexString13() == null
						|| cargoReportUnitDTO.getUnitFlexString13().isEmpty()) {
					return false;
				}

				String nrItem = cargoReportUnitDTO.getUnitFlexString14();

				if (nrItem == null || nrItem.isEmpty()) {
					return false;
				}
			}
		} else {
			return false;
		}

		if (validateSeal) {
			if (cargoReportUnitDTO.getSealNbr1() != null) {
				return true;
			}
			if (cargoReportUnitDTO.getSealNbr2() != null) {
				return true;
			}
			if (cargoReportUnitDTO.getSealNbr3() != null) {
				return true;
			}
			if (cargoReportUnitDTO.getSealNbr4() != null) {
				return true;
			}

			return false;
		}

		return true;
	}

	@Override
	public String execute(String localArquivo, String unitGKey) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
