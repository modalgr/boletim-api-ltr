
package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de conteinerWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="conteinerWSVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}itemCargaDescargaWSVO">
 *       &lt;sequence>
 *         &lt;element name="nrConteiner">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nusLacresConteiner" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nuLacreConteiner" maxOccurs="4">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="15"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "conteinerWSVO", namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", propOrder = {
    "nrConteiner",
    "nusLacresConteiner"
})
public class ConteinerWSVO
    extends ItemCargaDescargaWSVO
{

    @XmlElement(namespace = "", required = true)
    protected String nrConteiner;
    @XmlElement(namespace = "")
    protected ConteinerWSVO.NusLacresConteiner nusLacresConteiner;

    /**
     * Obt�m o valor da propriedade nrConteiner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrConteiner() {
        return nrConteiner;
    }

    /**
     * Define o valor da propriedade nrConteiner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrConteiner(String value) {
        this.nrConteiner = value;
    }

    /**
     * Obt�m o valor da propriedade nusLacresConteiner.
     * 
     * @return
     *     possible object is
     *     {@link ConteinerWSVO.NusLacresConteiner }
     *     
     */
    public ConteinerWSVO.NusLacresConteiner getNusLacresConteiner() {
        return nusLacresConteiner;
    }

    /**
     * Define o valor da propriedade nusLacresConteiner.
     * 
     * @param value
     *     allowed object is
     *     {@link ConteinerWSVO.NusLacresConteiner }
     *     
     */
    public void setNusLacresConteiner(ConteinerWSVO.NusLacresConteiner value) {
        this.nusLacresConteiner = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nuLacreConteiner" maxOccurs="4">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="15"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nuLacreConteiner"
    })
    public static class NusLacresConteiner {

        @XmlElement(namespace = "", required = true)
        protected List<String> nuLacreConteiner;

        /**
         * Gets the value of the nuLacreConteiner property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nuLacreConteiner property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNuLacreConteiner().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getNuLacreConteiner() {
            if (nuLacreConteiner == null) {
                nuLacreConteiner = new ArrayList<String>();
            }
            return this.nuLacreConteiner;
        }

    }

}
