//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de granelWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="granelWSVO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}operacaoCargaDescargaWSVO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cdTipoGranel"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="pbOperacaoKg"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;minExclusive value="1"/&gt;
 *               &lt;totalDigits value="12"/&gt;
 *               &lt;fractionDigits value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "granelWSVO", propOrder = {
    "cdTipoGranel",
    "pbOperacaoKg"
})
public class GranelWSVO
    extends OperacaoCargaDescargaWSVO
{

    @XmlElement(required = true)
    protected String cdTipoGranel;
    @XmlElement(required = true)
    protected BigDecimal pbOperacaoKg;

    /**
     * Obtém o valor da propriedade cdTipoGranel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdTipoGranel() {
        return cdTipoGranel;
    }

    /**
     * Define o valor da propriedade cdTipoGranel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdTipoGranel(String value) {
        this.cdTipoGranel = value;
    }

    /**
     * Obtém o valor da propriedade pbOperacaoKg.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPbOperacaoKg() {
        return pbOperacaoKg;
    }

    /**
     * Define o valor da propriedade pbOperacaoKg.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPbOperacaoKg(BigDecimal value) {
        this.pbOperacaoKg = value;
    }

}
