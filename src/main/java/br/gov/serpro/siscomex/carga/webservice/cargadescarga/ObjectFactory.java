//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.serpro.siscomex.carga.webservice.cargadescarga package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CargaDescargaResponse_QNAME = new QName("http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", "cargaDescargaResponse");
    private final static QName _CargaDescargaWSVO_QNAME = new QName("http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", "cargaDescargaWSVO");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.serpro.siscomex.carga.webservice.cargadescarga
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConteinerWSVO }
     * 
     */
    public ConteinerWSVO createConteinerWSVO() {
        return new ConteinerWSVO();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO }
     * 
     */
    public CargaDescargaWSVO createCargaDescargaWSVO() {
        return new CargaDescargaWSVO();
    }

    /**
     * Create an instance of {@link CargaDescargaResponse }
     * 
     */
    public CargaDescargaResponse createCargaDescargaResponse() {
        return new CargaDescargaResponse();
    }

    /**
     * Create an instance of {@link OperacaoCargaDescargaWSVO }
     * 
     */
    public OperacaoCargaDescargaWSVO createOperacaoCargaDescargaWSVO() {
        return new OperacaoCargaDescargaWSVO();
    }

    /**
     * Create an instance of {@link CargaSoltaWSVO }
     * 
     */
    public CargaSoltaWSVO createCargaSoltaWSVO() {
        return new CargaSoltaWSVO();
    }

    /**
     * Create an instance of {@link GranelWSVO }
     * 
     */
    public GranelWSVO createGranelWSVO() {
        return new GranelWSVO();
    }

    /**
     * Create an instance of {@link SobressalenteWSVO }
     * 
     */
    public SobressalenteWSVO createSobressalenteWSVO() {
        return new SobressalenteWSVO();
    }

    /**
     * Create an instance of {@link ItemCargaDescargaWSVO }
     * 
     */
    public ItemCargaDescargaWSVO createItemCargaDescargaWSVO() {
        return new ItemCargaDescargaWSVO();
    }

    /**
     * Create an instance of {@link VeiculoWSVO }
     * 
     */
    public VeiculoWSVO createVeiculoWSVO() {
        return new VeiculoWSVO();
    }

    /**
     * Create an instance of {@link ConteinerWSVO.NusLacresConteiner }
     * 
     */
    public ConteinerWSVO.NusLacresConteiner createConteinerWSVONusLacresConteiner() {
        return new ConteinerWSVO.NusLacresConteiner();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO.Conteineres }
     * 
     */
    public CargaDescargaWSVO.Conteineres createCargaDescargaWSVOConteineres() {
        return new CargaDescargaWSVO.Conteineres();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO.Veiculos }
     * 
     */
    public CargaDescargaWSVO.Veiculos createCargaDescargaWSVOVeiculos() {
        return new CargaDescargaWSVO.Veiculos();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO.Sobressalentes }
     * 
     */
    public CargaDescargaWSVO.Sobressalentes createCargaDescargaWSVOSobressalentes() {
        return new CargaDescargaWSVO.Sobressalentes();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO.Cargassoltas }
     * 
     */
    public CargaDescargaWSVO.Cargassoltas createCargaDescargaWSVOCargassoltas() {
        return new CargaDescargaWSVO.Cargassoltas();
    }

    /**
     * Create an instance of {@link CargaDescargaWSVO.Graneis }
     * 
     */
    public CargaDescargaWSVO.Graneis createCargaDescargaWSVOGraneis() {
        return new CargaDescargaWSVO.Graneis();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargaDescargaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", name = "cargaDescargaResponse")
    public JAXBElement<CargaDescargaResponse> createCargaDescargaResponse(CargaDescargaResponse value) {
        return new JAXBElement<CargaDescargaResponse>(_CargaDescargaResponse_QNAME, CargaDescargaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargaDescargaWSVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", name = "cargaDescargaWSVO")
    public JAXBElement<CargaDescargaWSVO> createCargaDescargaWSVO(CargaDescargaWSVO value) {
        return new JAXBElement<CargaDescargaWSVO>(_CargaDescargaWSVO_QNAME, CargaDescargaWSVO.class, null, value);
    }

}
