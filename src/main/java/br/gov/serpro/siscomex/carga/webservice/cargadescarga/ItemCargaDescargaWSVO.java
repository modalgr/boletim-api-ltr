//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de itemCargaDescargaWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="itemCargaDescargaWSVO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}operacaoCargaDescargaWSVO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="inAvariaOcorrencia"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="S"/&gt;
 *               &lt;enumeration value="N"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="nrManifesto" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="13"/&gt;
 *               &lt;maxLength value="13"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="nrConhecimento" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="15"/&gt;
 *               &lt;maxLength value="15"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="nrItem" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9][0-9][0-9][0-9]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itemCargaDescargaWSVO", propOrder = {
    "inAvariaOcorrencia",
    "nrManifesto",
    "nrConhecimento",
    "nrItem"
})
@XmlSeeAlso({
    CargaSoltaWSVO.class,
    VeiculoWSVO.class,
    ConteinerWSVO.class
})
public class ItemCargaDescargaWSVO
    extends OperacaoCargaDescargaWSVO
{

    @XmlElement(required = true)
    protected String inAvariaOcorrencia;
    protected String nrManifesto;
    protected String nrConhecimento;
    protected String nrItem;

    /**
     * Obtém o valor da propriedade inAvariaOcorrencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInAvariaOcorrencia() {
        return inAvariaOcorrencia;
    }

    /**
     * Define o valor da propriedade inAvariaOcorrencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInAvariaOcorrencia(String value) {
        this.inAvariaOcorrencia = value;
    }

    /**
     * Obtém o valor da propriedade nrManifesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrManifesto() {
        return nrManifesto;
    }

    /**
     * Define o valor da propriedade nrManifesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrManifesto(String value) {
        this.nrManifesto = value;
    }

    /**
     * Obtém o valor da propriedade nrConhecimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrConhecimento() {
        return nrConhecimento;
    }

    /**
     * Define o valor da propriedade nrConhecimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrConhecimento(String value) {
        this.nrConhecimento = value;
    }

    /**
     * Obtém o valor da propriedade nrItem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrItem() {
        return nrItem;
    }

    /**
     * Define o valor da propriedade nrItem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrItem(String value) {
        this.nrItem = value;
    }

}
