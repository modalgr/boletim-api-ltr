//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de operacaoCargaDescargaWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="operacaoCargaDescargaWSVO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cdOperacao"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="C"/&gt;
 *               &lt;enumeration value="D"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="cdTipoMovimentacao" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-0][0-3]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operacaoCargaDescargaWSVO", propOrder = {
    "cdOperacao",
    "cdTipoMovimentacao"
})
@XmlSeeAlso({
    GranelWSVO.class,
    SobressalenteWSVO.class,
    ItemCargaDescargaWSVO.class
})
public class OperacaoCargaDescargaWSVO {

    @XmlElement(required = true)
    protected String cdOperacao;
    protected String cdTipoMovimentacao;

    /**
     * Obtém o valor da propriedade cdOperacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdOperacao() {
        return cdOperacao;
    }

    /**
     * Define o valor da propriedade cdOperacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdOperacao(String value) {
        this.cdOperacao = value;
    }

    /**
     * Obtém o valor da propriedade cdTipoMovimentacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdTipoMovimentacao() {
        return cdTipoMovimentacao;
    }

    /**
     * Define o valor da propriedade cdTipoMovimentacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdTipoMovimentacao(String value) {
        this.cdTipoMovimentacao = value;
    }

}
