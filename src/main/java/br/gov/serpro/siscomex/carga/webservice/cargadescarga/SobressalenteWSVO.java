//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de sobressalenteWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="sobressalenteWSVO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}operacaoCargaDescargaWSVO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="txDescricao"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="500"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sobressalenteWSVO", propOrder = {
    "txDescricao"
})
public class SobressalenteWSVO
    extends OperacaoCargaDescargaWSVO
{

    @XmlElement(required = true)
    protected String txDescricao;

    /**
     * Obtém o valor da propriedade txDescricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxDescricao() {
        return txDescricao;
    }

    /**
     * Define o valor da propriedade txDescricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxDescricao(String value) {
        this.txDescricao = value;
    }

}
