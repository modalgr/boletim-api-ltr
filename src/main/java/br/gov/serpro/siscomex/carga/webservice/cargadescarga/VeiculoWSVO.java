//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.11.03 às 10:35:26 PM BRST 
//


package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de veiculoWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="veiculoWSVO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}itemCargaDescargaWSVO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nrChassiVeiculo"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="30"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "veiculoWSVO", propOrder = {
    "nrChassiVeiculo"
})
public class VeiculoWSVO
    extends ItemCargaDescargaWSVO
{

    @XmlElement(required = true)
    protected String nrChassiVeiculo;

    /**
     * Obtém o valor da propriedade nrChassiVeiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrChassiVeiculo() {
        return nrChassiVeiculo;
    }

    /**
     * Define o valor da propriedade nrChassiVeiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrChassiVeiculo(String value) {
        this.nrChassiVeiculo = value;
    }

}
