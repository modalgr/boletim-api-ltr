
package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de cargaSoltaWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="cargaSoltaWSVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}itemCargaDescargaWSVO">
 *       &lt;sequence>
 *         &lt;element name="qtOperacao">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;minInclusive value="0"/>
 *               &lt;totalDigits value="7"/>
 *               &lt;fractionDigits value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cargaSoltaWSVO", namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", propOrder = {
    "qtOperacao"
})
public class CargaSoltaWSVO
    extends ItemCargaDescargaWSVO
{

    @XmlElement(namespace = "", required = true)
    protected BigDecimal qtOperacao;

    /**
     * Obt�m o valor da propriedade qtOperacao.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQtOperacao() {
        return qtOperacao;
    }

    /**
     * Define o valor da propriedade qtOperacao.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQtOperacao(BigDecimal value) {
        this.qtOperacao = value;
    }

}
