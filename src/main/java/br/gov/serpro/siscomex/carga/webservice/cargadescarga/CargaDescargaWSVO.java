
package br.gov.serpro.siscomex.carga.webservice.cargadescarga;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.w3._2000._09.xmldsig_.SignatureType;


/**
 * <p>Classe Java de cargaDescargaWSVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="cargaDescargaWSVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nrEscala">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="11"/>
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cdTerminalOperacao">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cdTipoItem">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;pattern value="[0-4]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="conteineres" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="conteiner" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}conteinerWSVO" maxOccurs="50"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="veiculos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="veiculo" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}veiculoWSVO" maxOccurs="50"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sobressalentes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="sobressalente" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}sobressalenteWSVO" maxOccurs="2"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cargassoltas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="cargasolta" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}cargaSoltaWSVO" maxOccurs="50"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="graneis" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="granel" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}granelWSVO" maxOccurs="50"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cargaDescargaWSVO", namespace = "http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/", propOrder = {
    "nrEscala",
    "cdTerminalOperacao",
    "cdTipoItem",
    "conteineres",
    "veiculos",
    "sobressalentes",
    "cargassoltas",
    "graneis",
    "signature"
})
public class CargaDescargaWSVO {

    @XmlElement(namespace = "", required = true)
    protected String nrEscala;
    @XmlElement(namespace = "", required = true)
    protected String cdTerminalOperacao;
    @XmlElement(namespace = "")
    protected int cdTipoItem;
    @XmlElement(namespace = "")
    protected CargaDescargaWSVO.Conteineres conteineres;
    @XmlElement(namespace = "")
    protected CargaDescargaWSVO.Veiculos veiculos;
    @XmlElement(namespace = "")
    protected CargaDescargaWSVO.Sobressalentes sobressalentes;
    @XmlElement(namespace = "")
    protected CargaDescargaWSVO.Cargassoltas cargassoltas;
    @XmlElement(namespace = "")
    protected CargaDescargaWSVO.Graneis graneis;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;

    /**
     * Obt�m o valor da propriedade nrEscala.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrEscala() {
        return nrEscala;
    }

    /**
     * Define o valor da propriedade nrEscala.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrEscala(String value) {
        this.nrEscala = value;
    }

    /**
     * Obt�m o valor da propriedade cdTerminalOperacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdTerminalOperacao() {
        return cdTerminalOperacao;
    }

    /**
     * Define o valor da propriedade cdTerminalOperacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdTerminalOperacao(String value) {
        this.cdTerminalOperacao = value;
    }

    /**
     * Obt�m o valor da propriedade cdTipoItem.
     * 
     */
    public int getCdTipoItem() {
        return cdTipoItem;
    }

    /**
     * Define o valor da propriedade cdTipoItem.
     * 
     */
    public void setCdTipoItem(int value) {
        this.cdTipoItem = value;
    }

    /**
     * Obt�m o valor da propriedade conteineres.
     * 
     * @return
     *     possible object is
     *     {@link CargaDescargaWSVO.Conteineres }
     *     
     */
    public CargaDescargaWSVO.Conteineres getConteineres() {
        return conteineres;
    }

    /**
     * Define o valor da propriedade conteineres.
     * 
     * @param value
     *     allowed object is
     *     {@link CargaDescargaWSVO.Conteineres }
     *     
     */
    public void setConteineres(CargaDescargaWSVO.Conteineres value) {
        this.conteineres = value;
    }

    /**
     * Obt�m o valor da propriedade veiculos.
     * 
     * @return
     *     possible object is
     *     {@link CargaDescargaWSVO.Veiculos }
     *     
     */
    public CargaDescargaWSVO.Veiculos getVeiculos() {
        return veiculos;
    }

    /**
     * Define o valor da propriedade veiculos.
     * 
     * @param value
     *     allowed object is
     *     {@link CargaDescargaWSVO.Veiculos }
     *     
     */
    public void setVeiculos(CargaDescargaWSVO.Veiculos value) {
        this.veiculos = value;
    }

    /**
     * Obt�m o valor da propriedade sobressalentes.
     * 
     * @return
     *     possible object is
     *     {@link CargaDescargaWSVO.Sobressalentes }
     *     
     */
    public CargaDescargaWSVO.Sobressalentes getSobressalentes() {
        return sobressalentes;
    }

    /**
     * Define o valor da propriedade sobressalentes.
     * 
     * @param value
     *     allowed object is
     *     {@link CargaDescargaWSVO.Sobressalentes }
     *     
     */
    public void setSobressalentes(CargaDescargaWSVO.Sobressalentes value) {
        this.sobressalentes = value;
    }

    /**
     * Obt�m o valor da propriedade cargassoltas.
     * 
     * @return
     *     possible object is
     *     {@link CargaDescargaWSVO.Cargassoltas }
     *     
     */
    public CargaDescargaWSVO.Cargassoltas getCargassoltas() {
        return cargassoltas;
    }

    /**
     * Define o valor da propriedade cargassoltas.
     * 
     * @param value
     *     allowed object is
     *     {@link CargaDescargaWSVO.Cargassoltas }
     *     
     */
    public void setCargassoltas(CargaDescargaWSVO.Cargassoltas value) {
        this.cargassoltas = value;
    }

    /**
     * Obt�m o valor da propriedade graneis.
     * 
     * @return
     *     possible object is
     *     {@link CargaDescargaWSVO.Graneis }
     *     
     */
    public CargaDescargaWSVO.Graneis getGraneis() {
        return graneis;
    }

    /**
     * Define o valor da propriedade graneis.
     * 
     * @param value
     *     allowed object is
     *     {@link CargaDescargaWSVO.Graneis }
     *     
     */
    public void setGraneis(CargaDescargaWSVO.Graneis value) {
        this.graneis = value;
    }

    /**
     * Obt�m o valor da propriedade signature.
     * 
     * @return
     *     possible object is
     *     {@link SignatureType }
     *     
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Define o valor da propriedade signature.
     * 
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *     
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cargasolta" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}cargaSoltaWSVO" maxOccurs="50"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cargasolta"
    })
    public static class Cargassoltas {

        @XmlElement(namespace = "", required = true)
        protected List<CargaSoltaWSVO> cargasolta;

        /**
         * Gets the value of the cargasolta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cargasolta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCargasolta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CargaSoltaWSVO }
         * 
         * 
         */
        public List<CargaSoltaWSVO> getCargasolta() {
            if (cargasolta == null) {
                cargasolta = new ArrayList<CargaSoltaWSVO>();
            }
            return this.cargasolta;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="conteiner" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}conteinerWSVO" maxOccurs="50"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "conteiner"
    })
    public static class Conteineres {

        @XmlElement(namespace = "", required = true)
        protected List<ConteinerWSVO> conteiner;

        /**
         * Gets the value of the conteiner property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the conteiner property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConteiner().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ConteinerWSVO }
         * 
         * 
         */
        public List<ConteinerWSVO> getConteiner() {
            if (conteiner == null) {
                conteiner = new ArrayList<ConteinerWSVO>();
            }
            return this.conteiner;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="granel" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}granelWSVO" maxOccurs="50"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "granel"
    })
    public static class Graneis {

        @XmlElement(namespace = "", required = true)
        protected List<GranelWSVO> granel;

        /**
         * Gets the value of the granel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the granel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGranel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GranelWSVO }
         * 
         * 
         */
        public List<GranelWSVO> getGranel() {
            if (granel == null) {
                granel = new ArrayList<GranelWSVO>();
            }
            return this.granel;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="sobressalente" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}sobressalenteWSVO" maxOccurs="2"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sobressalente"
    })
    public static class Sobressalentes {

        @XmlElement(namespace = "", required = true)
        protected List<SobressalenteWSVO> sobressalente;

        /**
         * Gets the value of the sobressalente property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the sobressalente property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSobressalente().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SobressalenteWSVO }
         * 
         * 
         */
        public List<SobressalenteWSVO> getSobressalente() {
            if (sobressalente == null) {
                sobressalente = new ArrayList<SobressalenteWSVO>();
            }
            return this.sobressalente;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="veiculo" type="{http://cargadescarga.webservice.carga.siscomex.serpro.gov.br/}veiculoWSVO" maxOccurs="50"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "veiculo"
    })
    public static class Veiculos {

        @XmlElement(namespace = "", required = true)
        protected List<VeiculoWSVO> veiculo;

        /**
         * Gets the value of the veiculo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the veiculo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getVeiculo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link VeiculoWSVO }
         * 
         * 
         */
        public List<VeiculoWSVO> getVeiculo() {
            if (veiculo == null) {
                veiculo = new ArrayList<VeiculoWSVO>();
            }
            return this.veiculo;
        }

    }

}
